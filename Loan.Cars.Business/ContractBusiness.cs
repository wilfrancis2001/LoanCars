﻿
using Loan.Cars.Model;
using System.Collections.Generic;

namespace Loan.Cars.Business
{
    public class ContractBusiness
    {
        private Repositories.ContractRepository contractTable;
        public ContractBusiness(System.Data.Common.DbConnection _con) 
        {
            contractTable = new Repositories.ContractRepository(_con); 
        }

        public IEnumerable<Contract> GetContracts()
        {
            return contractTable.GetContracts();
        }

        public Contract GetContractById(int id) 
        {
            return contractTable.GetContractById(id);
        }

        public void AddContract(Contract agence) 
        {
            contractTable.AddContract(agence);
        }

        public void UpdateContract(Contract agence)
        {
            contractTable.UpdateContract(agence);
        }

        public void DeleteContract(int id)  
        {
            contractTable.DeleteContract(id);
        }
    }
}
