﻿
using System;
using System.Threading.Tasks;
using System.Data.Common;
using Microsoft.AspNet.Identity;
using System.Linq;
using Loan.Cars.Repositories;

namespace Loan.Cars.Business
{
    public class RoleStore<TRole> : IQueryableRoleStore<TRole> where TRole : IRole
    {
        private RoleRepository<TRole> RoleRepository;

        public RoleStore(DbConnection _conn)
        {
            RoleRepository = new RoleRepository<TRole>(_conn);
        }

        public IQueryable<TRole> Roles =>  RoleRepository.GetRoles().AsQueryable<TRole>(); 

        public Task CreateAsync(TRole role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("role");
            }
            return new Task(() => RoleRepository.Insert(role));
        }

        public Task UpdateAsync(TRole role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("role");
            }
            return new Task(() => RoleRepository.Update(role));
        }

        public Task DeleteAsync(TRole role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("role");
            }
            return new Task(() => RoleRepository.Delete(role));
        }

        public Task<TRole> FindByIdAsync(string roleId)
        {
            return Task.FromResult(RoleRepository.GetRoleById(roleId));
        }

        public Task<TRole> FindByNameAsync(string roleName)
        {
            return Task.FromResult(RoleRepository.GetRoleByName(roleName));
        }

        public void Dispose()
        {
            //to do
        }
    }
}
