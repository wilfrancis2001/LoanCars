﻿
using System;
using System.Linq;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using System.Data.Common;
using Loan.Cars.Model;
using Loan.Cars.Repositories;

namespace Loan.Cars.Business
{
    public class UserStore<TUser, TRole> : IUserLoginStore<TUser>,
        IUserClaimStore<TUser>,
        IUserRoleStore<TUser>,
        IUserPasswordStore<TUser>,
        IUserSecurityStampStore<TUser>,        
        IUserEmailStore<TUser>,
        IUserPhoneNumberStore<TUser>,
        IUserTwoFactorStore<TUser, string>,
        IUserLockoutStore<TUser, string>,
        IUserStore<TUser>
        where TUser : class, IDapperIdentity<TUser>
        where TRole : IRole 

    {
        private UserRepository<TUser> userTable;
        private RoleRepository<TRole> roleTable;
        private UserRolesRepository<TUser> userRolesTable;
        private UserClaimsRepository<TUser> userClaimsTable;
        private UserLoginsRepository<TUser> userLoginsTable;

        public UserStore(DbConnection _connection)
        {
            userTable = new UserRepository<TUser>(_connection);
            roleTable = new RoleRepository<TRole>(_connection);
            userRolesTable = new UserRolesRepository<TUser>(_connection);
            userClaimsTable = new UserClaimsRepository<TUser>(_connection);
            userLoginsTable = new UserLoginsRepository<TUser>(_connection);
        }

        public IQueryable<TUser> Users => userTable.GetUsers().AsQueryable();

        public Task AddClaimAsync(TUser user, Claim claim)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (claim == null)
            {
                throw new ArgumentNullException("user");
            }

            return new Task(() => userClaimsTable.Insert(user, claim));
        }

        public Task AddLoginAsync(TUser user, UserLoginInfo login)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (login == null)
            {
                throw new ArgumentNullException("login");
            }
            return new Task(() => userLoginsTable.Insert(user, login));
        }

        public Task AddToRoleAsync(TUser user, string roleName)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (string.IsNullOrEmpty(roleName))
            {
                throw new ArgumentException("Argument cannot be null or empty: roleName.");
            }

            var identRole = roleTable.GetRoleByName(roleName);
            if (!string.IsNullOrEmpty(identRole.Id))
            {
                userRolesTable.Insert(user.Id, identRole.Id);
            }

            return Task.FromResult<object>(null);
        }

        public Task CreateAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            //return new Task(() => userTable.Insert(user));
            userTable.Insert(user);
            return Task.FromResult(0);
        }

        public Task DeleteAsync(TUser user)
        {
            if (user != null)
            {
                userTable.Delete(user);
            }
            return Task.FromResult<Object>(null);
        }

        public Task<TUser> FindAsync(UserLoginInfo login)
        {
            if (login == null)
            {
                throw new ArgumentNullException("login");
            }
            var userId = userLoginsTable.FindUserIdbyLogin(login);
            TUser user = userTable.GetUserById(userId) as TUser;
            return Task.FromResult<TUser>(user);
        }

        public Task<TUser> FindByEmailAsync(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                throw new ArgumentException("Null or empty argument: email");
            }

            TUser user = userTable.GetUserByEmail(email);
            return Task.FromResult<TUser>(user);
        }

        public Task<TUser> FindByIdAsync(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new ArgumentException("Null or empty argument: userId");
            }

            TUser result = userTable.GetUserById(userId) as TUser;
            return Task.FromResult<TUser>(result);
        }

        public Task<TUser> FindByNameAsync(string userName)
        {
            if (string.IsNullOrEmpty(userName))
            {
                throw new ArgumentException("Null or empty argument: userName");
            }

            var result = userTable.GetUserByName(userName);
            return Task.FromResult<TUser>(result);
        }

        public Task<int> GetAccessFailedCountAsync(TUser user)
        {
            return Task.FromResult(user.AccessFailedCount);
        }

        public Task<IList<Claim>> GetClaimsAsync(TUser user)
        {
            var claims = userClaimsTable.FindByUserId(user.Id).ToList();
            return Task.FromResult<IList<Claim>>(claims);
        }

        public Task<string> GetEmailAsync(TUser user)
        {
            return Task.FromResult(user?.Email);
        }

        public Task<bool> GetEmailConfirmedAsync(TUser user)
        {
            return Task.FromResult(user != null ? user.EmailConfirmed : false);
        }

        public Task<bool> GetLockoutEnabledAsync(TUser user)
        {
            return Task.FromResult(user != null ? user.LockoutEnabled : false);
        }

        public Task<DateTimeOffset> GetLockoutEndDateAsync(TUser user)
        {
            return Task.FromResult(user.LockoutEndDateUtc.HasValue
                              ? new DateTimeOffset(DateTime.SpecifyKind(user.LockoutEndDateUtc.Value, DateTimeKind.Utc))
                              : new DateTimeOffset());
        }

        public Task<IList<UserLoginInfo>> GetLoginsAsync(TUser user)
        {            
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            var logins = userLoginsTable.FindAllByUserId(user.Id).ToList();
            return Task.FromResult<IList<UserLoginInfo>>(logins);
        }

        public Task<string> GetPasswordHashAsync(TUser user)
        {
            //string passwordHash = userTable.GetPasswordHash(user.Id);
            return Task.FromResult<string>(user?.PasswordHash);
        }

        public Task<string> GetPhoneNumberAsync(TUser user)
        {
            return Task.FromResult(user?.PhoneNumber);
        }

        public Task<bool> GetPhoneNumberConfirmedAsync(TUser user)
        {
            return Task.FromResult(user != null ? user.PhoneNumberConfirmed : false);
        }

        public Task<IList<string>> GetRolesAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            var userroles = userRolesTable.FindByUserId(user.Id).ToList(); 
            return Task.FromResult<IList<string>>(userroles.Select(x=>x.RoleName).ToList());
        }

        public Task<string> GetSecurityStampAsync(TUser user)
        {
            return Task.FromResult(user?.SecurityStamp);
        }

        public Task<bool> GetTwoFactorEnabledAsync(TUser user)
        {
            return Task.FromResult(user != null ? user.TwoFactorEnabled : false);
        }

        public Task<bool> HasPasswordAsync(TUser user)
        {
            //var hasPassword = !string.IsNullOrEmpty(userTable.GetPasswordHash(user.Id));
            //return Task.FromResult<bool>(Boolean.Parse(hasPassword.ToString()));
            return Task.FromResult<bool>(!string.IsNullOrEmpty(user?.PasswordHash));
        }

        public Task<int> IncrementAccessFailedCountAsync(TUser user)
        {
            user.AccessFailedCount++;
            userTable.Update(user);

            return Task.FromResult<int>(user.AccessFailedCount);
        }

        public Task<bool> IsInRoleAsync(TUser user, string roleName)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            if (string.IsNullOrEmpty(roleName))
            {
                throw new ArgumentNullException("role");
            }

            var userroles = userRolesTable.FindByUserId(user.Id); 
            return Task.FromResult<bool>(userroles != null && userroles.Select(x=>x.RoleName).Contains(roleName));
        }

        public Task RemoveClaimAsync(TUser user, Claim claim)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            if (claim == null)
            {
                throw new ArgumentNullException("claim");
            }

            return new Task(() => userClaimsTable.DeleteSingleclaim(user, claim));
        }

        public Task RemoveFromRoleAsync(TUser user, string roleName)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            if (string.IsNullOrWhiteSpace(roleName))
            {
                throw new ArgumentNullException("roleName");
            }

            return new Task(() => userRolesTable.RemoveUserFromRole(user.Id, roleName));
        }

        public Task RemoveLoginAsync(TUser user, UserLoginInfo login)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            if (login == null)
            {
                throw new ArgumentNullException("login");
            }
            return new Task(() => userLoginsTable.Delete(user, login));
        }

        public Task ResetAccessFailedCountAsync(TUser user)
        {
            user.AccessFailedCount = 0;
            return UpdateAsync(user);
        }

        public Task SetEmailAsync(TUser user, string email)
        {
            user.Email = email;
            return UpdateAsync(user);
        }

        public Task SetEmailConfirmedAsync(TUser user, bool confirmed)
        {
            user.EmailConfirmed = confirmed;
            return UpdateAsync(user);
        }

        public Task SetLockoutEnabledAsync(TUser user, bool enabled)
        {
            user.LockoutEnabled = enabled;
            return UpdateAsync(user);
        }

        public Task SetLockoutEndDateAsync(TUser user, DateTimeOffset lockoutEnd)
        {
            user.LockoutEndDateUtc = lockoutEnd.UtcDateTime;
            return UpdateAsync(user);
        }

        public Task SetPasswordHashAsync(TUser user, string passwordHash)
        {
            user.PasswordHash = passwordHash;
            return UpdateAsync(user);
        }

        public Task SetPhoneNumberAsync(TUser user, string phoneNumber)
        {
            user.PhoneNumber = phoneNumber;
            return UpdateAsync(user);
        }

        public Task SetPhoneNumberConfirmedAsync(TUser user, bool confirmed)
        {
            user.PhoneNumberConfirmed = confirmed;
            return UpdateAsync(user);
        }

        public Task SetSecurityStampAsync(TUser user, string stamp)
        {
            user.SecurityStamp = stamp;
            return UpdateAsync(user);
        }

        public Task SetTwoFactorEnabledAsync(TUser user, bool enabled)
        {
            user.TwoFactorEnabled = enabled;
            return UpdateAsync(user);
        }

        public Task UpdateAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            userTable.Update(user);
            return Task.FromResult(0);
        }


        public void Dispose()
        {
            
        }

    }
}
