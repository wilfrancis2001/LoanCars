﻿
using Loan.Cars.Model;
using System.Collections.Generic;

namespace Loan.Cars.Business
{
    public class BookingBusiness 
    {
        private Repositories.BookingRepository bookingTable;
        public BookingBusiness(System.Data.Common.DbConnection _con)
        {
            bookingTable = new Repositories.BookingRepository(_con);
        }

        public IEnumerable<Booking> GetBookings(string userid)
        {
            return bookingTable.GetBookings(userid);
        }

        public Booking GetBookingById(int id)
        {
            return bookingTable.GetBookingById(id);
        }

        public Booking GetBookingByCode(string code)
        {
            return bookingTable.GetBookingByCode(code); 
        }

        public void AddBooking(Booking booking)
        {
            bookingTable.AddBooking(booking);
        }

        public void UpdateBooking(Booking booking)
        {
            bookingTable.UpdateBooking(booking);
        }

        public void DeleteBooking(int id) 
        {
            bookingTable.DeleteBooking(id);
        }
    }
}
