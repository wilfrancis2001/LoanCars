﻿using Loan.Cars.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loan.Cars.Business
{
    public class CategorieVehiculeBusiness
    {
        private Repositories.CategorieVehiculeRepository categorieRepoTable; 
        public CategorieVehiculeBusiness(System.Data.Common.DbConnection _con)
        {
            categorieRepoTable = new Repositories.CategorieVehiculeRepository(_con); 
        }

        public IEnumerable<CategorieVehicule> GetCategorieVehicules()
        {
            return categorieRepoTable.GetCategorieVehicules();
        }

        public IEnumerable<CategorieVehicule> GetCategorieVehiculeByModelId(int id)
        {
            return categorieRepoTable.GetCategorieVehiculeByModelId(id);
        }


        public CategorieVehicule GetCategorieVehiculeByCode(string code) 
        {
            return categorieRepoTable.GetCategorieVehiculeByCode(code);
        }

        public bool IsInCategorie(string code)
        {
            return categorieRepoTable.IsInCategorie(code);
        }

        public void AddCategorieVehicule(CategorieVehicule cat)
        {
            categorieRepoTable.AddCategorieVehicule(cat);
        }
    }
}
