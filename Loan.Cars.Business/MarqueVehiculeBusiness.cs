﻿using Loan.Cars.Model;
using Loan.Cars.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loan.Cars.Business
{
    public class MarqueVehiculeBusiness
    {
        private MarqueVehiculeRepository marqueTable;
        public MarqueVehiculeBusiness(System.Data.IDbConnection _con) 
        {
            marqueTable = new MarqueVehiculeRepository(_con);
        }

        public IEnumerable<Marque> GetMarqueVehicules()
        {
            return marqueTable.GetMarqueVehicules();
        }

        public Marque GetMarqueVehiculeByCode(string code)
        {
            return marqueTable.GetMarqueVehiculeByCode(code);
        }

        public bool IsInMarque(string code)
        {
            return marqueTable.IsInMarque(code);
        }

        public void AddMarqueVehicule(Marque marque) 
        {
            marqueTable.AddMarqueVehicule(marque);
        }
    }
}
