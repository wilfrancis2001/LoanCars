﻿
using Loan.Cars.Model.Common;
using Loan.Cars.Repositories;
using System.Collections.Generic;
using System.Data.Common;

namespace Loan.Cars.Business
{
    public class UserBusiness 
    {
        private UserRepository<ApplicationUser> userTable;
        private UserRolesRepository<ApplicationUser> userroleTable;
        public UserBusiness(DbConnection _con) 
        {
            userTable = new UserRepository<ApplicationUser>(_con);
            userroleTable = new UserRolesRepository<ApplicationUser>(_con);
        }


        public IEnumerable<ApplicationUser> GetUsers(bool onlyStaff = true)
        {
            return userTable.GetUsers(onlyStaff);
        }

        public void RemoveFromRole(string userId,string roleName)
        {
            userroleTable.RemoveUserFromRole(userId, roleName);
        }
    }
}
