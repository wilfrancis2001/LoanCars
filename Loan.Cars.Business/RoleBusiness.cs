﻿using Loan.Cars.Model;
using Loan.Cars.Repositories;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace Loan.Cars.Business
{
    public class RoleBusiness
    {
        private RoleRepository<IdentityRole> roleTable;
        public RoleBusiness(DbConnection _con) 
        {
            roleTable = new RoleRepository<IdentityRole>(_con);
        }


        public bool IsInRole(string roleName)
        {
            return roleTable.IsInRole(roleName);
        }

        public void Delete(IdentityRole role)
        {
            roleTable.Delete(role);
        }

        public void Insert(IdentityRole role)
        {
            roleTable.Insert(role);
        }

        public IdentityRole GetRoleById(string roledId)
        {
            return roleTable.GetRoleById(roledId);
        }

        public IdentityRole GetRoleByName(string roleName)
        {
            return roleTable.GetRoleByName(roleName);
        }

        public void Update(IdentityRole role)
        {
            roleTable.Update(role);
        }

        public IEnumerable<IdentityRole> GetRoles() => roleTable.GetRoles();
    }
}
