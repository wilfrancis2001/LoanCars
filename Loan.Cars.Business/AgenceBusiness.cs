﻿
using Loan.Cars.Model;
using Loan.Cars.Repositories;
using System.Collections.Generic;
using System.Data.Common;

namespace Loan.Cars.Business
{
    public class AgenceBusiness
    {
        private AgenceRepository agenceTable; 
        public AgenceBusiness(DbConnection _con)
        {
            agenceTable = new AgenceRepository(_con);
        }

        public IEnumerable<Agence> GetAgences()
        {
            return agenceTable.GetAgcences();
        }

        public Agence GetAgenceById(int id)
        {
            return agenceTable.GetAgcenceById(id);
        }

        public void AddAgence(Agence agence)
        {
            agenceTable.AddAgcence(agence);
        }

        public void UpdateAgence(Agence agence)
        {
            agenceTable.UpdateAgcence(agence);
        }

        public void DeleteAgences(int id) 
        {
            agenceTable.DeleteAgcence(id);
        }
    }
}
