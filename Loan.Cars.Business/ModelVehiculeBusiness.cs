﻿using Loan.Cars.Model;
using Loan.Cars.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loan.Cars.Business
{
    public class ModeleVehiculeBusiness
    {
        private ModeleVehiculeRepository modeleTable;
        public ModeleVehiculeBusiness(System.Data.IDbConnection _con) 
        {
            modeleTable = new ModeleVehiculeRepository(_con);
        }

        public IEnumerable<Modele> GetModeleVehicules() 
        {
            return modeleTable.GetModeleVehicules();
        }

        public Modele GetModeleVehiculeByCode(string code) 
        {
            return modeleTable.GetModeleVehiculeByCode(code);
        }

        public IEnumerable<Modele> GetModeleVehiculeByMarqueId(int marqueId)   
        {
            return modeleTable.GetModeleVehiculeByMarqueId(marqueId);
        }

        public bool IsInModele(string code)
        {
            return modeleTable.IsInModele(code);
        }

        public void AddModeleVehicule(Modele modele)
        {
            modeleTable.AddModeleVehicule(modele);
        }
    }
}
