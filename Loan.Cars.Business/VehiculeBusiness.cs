﻿using Loan.Cars.Model;
using Loan.Cars.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loan.Cars.Business
{
    public class VehiculeBusiness
    {
        private VehiculeRepository VehiculeTable;

        public VehiculeBusiness(System.Data.Common.DbConnection _con)
        {
            VehiculeTable = new VehiculeRepository(_con);
        }

        public IEnumerable<Vehicule> GetVehicules(bool carAvailaible, bool isCar = true)
        {
            return VehiculeTable.GetVehicules(carAvailaible, isCar);
        }

        public Vehicule GetVehiculeById(int id)
        {
            return VehiculeTable.GetVehiculeById(id);
        }

        public void AddVehicule(Vehicule Vehicule)
        {
            VehiculeTable.AddVehicule(Vehicule);
        }

        public void UpdateVehicule(Vehicule Vehicule)
        {
            VehiculeTable.AddVehicule(Vehicule);
        }

        public void DeleteVehicules(int id)
        {
            VehiculeTable.DeleteVehicule(id);
        }
    }
}
