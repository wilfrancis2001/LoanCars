﻿
using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Reflection;

namespace Loan.Cars.Prerequisities
{
    public class ConnectionFactory 
    {
        #region Initialisation

        private const string Default_providerName = "System.Data.SqlClient";
        private NLog.ILogger Logger;
        private Func<MethodBase, string> GetCurrentMethodAndParameters;

        public ConnectionFactory()
        {
            Logger = new LoggerFactory().CreateLogger(GetType().FullName);
            GetCurrentMethodAndParameters = new LoggerFactory().GetCurrentMethodAndParameters;
        }

        #endregion

        public IDbConnection CreateConnection(string dbprovidername = Default_providerName) 
        {
            try
            {
                if (string.IsNullOrEmpty(dbprovidername))
                    throw new Exception("Provider name cannot null or empty string");

                DbConnection connection = null;
                string connectionString = GetConnectionStringByProvider(dbprovidername);

                if (!string.IsNullOrEmpty(connectionString))
                {
                    DbProviderFactory factory = DbProviderFactories.GetFactory(dbprovidername);
                    connection = factory.CreateConnection();
                    connection.ConnectionString = connectionString;

                }
                return connection;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, GetCurrentMethodAndParameters(MethodBase.GetCurrentMethod()));
                throw;
            }
        }

        private string GetConnectionStringByProvider(string providerName)
        {
            string returnValue = null;
            ConnectionStringSettingsCollection settings = ConfigurationManager.ConnectionStrings;
            if (settings != null)
            {
                foreach (ConnectionStringSettings cs in settings)
                {
                    if (cs.ProviderName == providerName)
                    {
                        returnValue = cs.ConnectionString;
                        break;
                    }
                }
            }
            return returnValue;
        }
    }

    public class ApplicationDbContext
    {
        public static System.Data.Common.DbConnection Create()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            return new System.Data.SqlClient.SqlConnection(connectionString);
        }
    }

}
