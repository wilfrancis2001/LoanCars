﻿using Dapper;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;

namespace Loan.Cars.Prerequisities
{
    public class BaseRepository : IDisposable
    {
        #region initilisation

        private ILogger Logger;
        protected IDbConnection Connection;

        protected BaseRepository(IDbConnection _con)
        {
            Logger = new LoggerFactory().CreateLogger(this.GetType().FullName);
            Connection = _con;
        }

        #endregion

        #region Connection managemment

        protected bool EnsureConnectionOpen()
        {
            bool isOpen = false;
            if (Connection != null)
            {
                if (Connection.State == ConnectionState.Open)
                {
                    isOpen = true;
                }
                else
                {
                    while (Connection.State != ConnectionState.Open)
                    {
                        Connection.Open();
                        isOpen = true;
                    }
                }
            }
            return isOpen;
        }

        protected void EnsureConnectionClosed()
        {
            if (Connection != null)
            {
                if (Connection.State == ConnectionState.Open)
                    Connection.Close();
            }
        }

        #endregion

        #region data base operation 

        protected virtual int ExecuteNonQuery(string scriptSqlOrStroredProcedureName, CommandType commandType, object parameters = null)
        {
            try
            {
                if (Connection == null) throw new Exception(ConstantRessources.__ERROR_CONNECTION_CANNOT_BE_NULL);
                if (!EnsureConnectionOpen()) throw new Exception(ConstantRessources.__ERROR_CONNECTION_CANNOT_BE_OPEN);
                return Connection.Execute(
                                            sql: scriptSqlOrStroredProcedureName,
                                            param: parameters,
                                            commandType:commandType
                                         );
            }
            catch (Exception ex)
            {
                Logger.Error(ex, $"[{System.Reflection.MethodBase.GetCurrentMethod().Name}]");
                throw;
            }
            finally
            {
                EnsureConnectionClosed();
            }
        }

        protected virtual T ExecuteScalar<T>(string scriptSqlOrStroredProcedureName, CommandType commandType, object parameters = null, Func<IDataReader, T> mapper = null)
        {
            try
            {
                if (Connection == null) throw new Exception(ConstantRessources.__ERROR_CONNECTION_CANNOT_BE_NULL);
                if (!EnsureConnectionOpen()) throw new Exception(ConstantRessources.__ERROR_CONNECTION_CANNOT_BE_OPEN);
                using (var reader = Connection.ExecuteReader(
                                                                sql:scriptSqlOrStroredProcedureName,
                                                                param: parameters,
                                                                commandType: commandType
                                                            )) 
                {
                    if (mapper == null)
                        mapper = reader.GetRowParser<T>(typeof(T));

                    while (reader.Read())
                        return mapper(reader);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, $"[{System.Reflection.MethodBase.GetCurrentMethod().Name}]");
                throw;
            }
            finally
            {
                EnsureConnectionClosed();
            }

            return default(T);
        }

        protected virtual IEnumerable<T> ExecuteSelect<T>(string scriptSqlOrStroredProcedureName, CommandType commandType, object parameters = null, Func<IDataReader, T> mapper = null)
        {
            try
            {
                if (Connection == null) throw new Exception(ConstantRessources.__ERROR_CONNECTION_CANNOT_BE_NULL);
                if (!EnsureConnectionOpen()) throw new Exception(ConstantRessources.__ERROR_CONNECTION_CANNOT_BE_OPEN);
                using (var reader = Connection.ExecuteReader(
                                                                sql: scriptSqlOrStroredProcedureName,
                                                                param: parameters,
                                                                commandType: commandType
                                                            ))
                {
                    var list = new List<T>();

                    if (mapper == null)
                        mapper = reader.GetRowParser<T>(typeof(T));

                    while (reader.Read())
                        list.Add(mapper(reader));

                    return list;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, $"[{System.Reflection.MethodBase.GetCurrentMethod().Name}]");
                throw;
            }
            finally
            {
                EnsureConnectionClosed();
            }
        }

        public void Dispose()
        {
            if (Connection != null)
            {
                if (Connection.State == ConnectionState.Open)
                    Connection.Close();

                Connection.Dispose();
            }
        }

        #endregion
    }
}
