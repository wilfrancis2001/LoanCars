﻿using System.Reflection;
using NLog;
using System.Text;

namespace Loan.Cars.Prerequisities
{
    public class LoggerFactory 
    {
        public ILogger CreateLogger(string LoggerName)
        {
            return LogManager.GetLogger(LoggerName);
        }

        public string GetCurrentMethodAndParameters(MethodBase methodBase)
        {
            StringBuilder builer = builer = new StringBuilder();
            ParameterInfo[] parameterInfos = methodBase.GetParameters();

            foreach (var info in parameterInfos)
            {
                if (builer.Length <= 0)
                    builer.Append(info.ToString());
                else
                {
                    builer.Append(", ");
                    builer.Append(info.ToString());
                }
            }

            if (builer.Length > 0)
                return $"{methodBase.Name}, Parameters:[ {builer.ToString()} ]";
            else
                return $"{methodBase.Name}";
        }
    }
}
