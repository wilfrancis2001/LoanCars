﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loan.Cars.Prerequisities
{
    public class ConstantRessources
    {
        public const string __ERROR_CONNECTION_CANNOT_BE_NULL = "Connection object cannot be null";
        public const string __ERROR_CONNECTION_CANNOT_BE_OPEN = "Connection cannot be open";
        public const string __PROVIDER_NAME = "System.Data.SqlClient";
    }
}
