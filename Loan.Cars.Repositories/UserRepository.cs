﻿using Loan.Cars.Model;
using Loan.Cars.Prerequisities;
using System;
using System.Collections.Generic;
using System.Data;

namespace Loan.Cars.Repositories
{
    public class UserRepository<TUser> : BaseRepository where TUser : class, IDapperIdentity<TUser>
    {
        public UserRepository(IDbConnection _con) : base(_con) { }

        public TUser GetUserById(string userId)
        {
            return ExecuteScalar<TUser>("sp_User_FindById", CommandType.StoredProcedure, new { Id = userId }); 
        }

        public TUser GetUserByName(string userName)
        {
            return ExecuteScalar<TUser>("sp_User_FindByName", CommandType.StoredProcedure, new { userName = userName }); 
        }

        public TUser GetUserByEmail(string email)
        {
            return ExecuteScalar<TUser>("sp_User_FindByEmail", CommandType.StoredProcedure, new { email = email });
        }

        public string GetPasswordHash(string userId)
        {
            return GetUserById(userId).PasswordHash;
        }

        public void Insert(TUser user)
        {
            ExecuteNonQuery("sp_User_Add", CommandType.StoredProcedure, new {
                Id = user.Id,
                AgenceId = user.AgenceId,
                Email = user.Email,
                UserName = user.UserName,
                FirstName = user.FirstName,
                LastName = user.LastName,
                PhoneNumber = user.PhoneNumber,
                StreetNumber = user.StreetNumber,
                StreetName = user.StreetName,
                AdditionnalInfo = user.AdditionnalInfo,
                BoxOffice = user.BoxOffice,
                Town = user.Town,
                Country = user.Country,
                PasswordHash = user.PasswordHash,
                SecurityStamp = user.SecurityStamp,
                LockoutEndDateUtc = user.LockoutEndDateUtc,
                CreationDate = DateTimeOffset.UtcNow
            });
        }

        public void Delete(TUser user)
        {
            ExecuteNonQuery("sp_User_DeleteById", CommandType.StoredProcedure, new { Id = user.Id });
        }

        public void Update(TUser user)
        {
            ExecuteNonQuery("sp_User_Update", CommandType.StoredProcedure, new
            {
                Id = user.Id,
                AgenceId = user.AgenceId,
                Email = user.Email,
                UserName = user.UserName,
                FirstName = user.FirstName,
                IsActive = user.IsActive,
                LastName = user.LastName,
                EmailConfirmed = user.EmailConfirmed,
                AccessFailedCount = user.AccessFailedCount,
                PhoneNumber = user.PhoneNumber,
                StreetNumber = user.StreetNumber,
                StreetName = user.StreetName,
                AdditionnalInfo = user.AdditionnalInfo,
                BoxOffice = user.BoxOffice,
                Town = user.Town,
                Country = user.Country,
                PasswordHash = user.PasswordHash,
                SecurityStamp = user.SecurityStamp,
                UpdateDate = DateTimeOffset.UtcNow
            });
        }

        public IEnumerable<TUser> GetUsers(bool onlyStaff = true)
        {
            if(!onlyStaff)
                return ExecuteSelect<TUser>("sp_User_Select", CommandType.StoredProcedure);
            else return ExecuteSelect<TUser>("sp_Employees_Get", CommandType.StoredProcedure);
        }
    }
}