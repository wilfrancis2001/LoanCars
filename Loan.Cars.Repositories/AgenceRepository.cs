﻿
using Loan.Cars.Prerequisities;
using System.Collections.Generic;
using System.Data;
using Loan.Cars.Model;

namespace Loan.Cars.Repositories
{
    public class AgenceRepository : BaseRepository
    {
        public AgenceRepository(IDbConnection _con) : base(_con) { }

        public IEnumerable<Agence> GetAgcences()
        {
            return ExecuteSelect<Agence>("sp_Agence_Select", CommandType.StoredProcedure);
        }

        public Agence GetAgcenceById(int id)
        {
            return ExecuteScalar<Agence>("sp_Agence_FindById", CommandType.StoredProcedure, new { id = id });
        }

        public void AddAgcence(Agence agence)
        {
            ExecuteNonQuery("sp_Agence_Add", CommandType.StoredProcedure, new
            {
                name = agence.Name,
                phoneNumber = agence.PhoneNumber,
                streetNumber = agence.StreetNumber,
                streetName = agence.StreetName,
                additioAdress = agence.AdditionAdress,
                zipCode = agence.ZipCode,
                town = agence.Town,
                country = agence.Country
            });
        }

        public void UpdateAgcence(Agence agence)
        {
            ExecuteNonQuery("sp_Agence_Update", CommandType.StoredProcedure, new
            {
                id = agence.Id,
                name = agence.Name,
                phoneNumber = agence.PhoneNumber,
                streetNumber = agence.StreetNumber,
                streetName = agence.StreetName,
                additioAdress = agence.AdditionAdress,
                zipCode = agence.ZipCode,
                town = agence.Town,
                country = agence.Country
            });
        }

        public void DeleteAgcence(int id)
        {
            ExecuteNonQuery("sp_Agence_DeleteById", CommandType.StoredProcedure, new { id = id });
        }

    }
}
