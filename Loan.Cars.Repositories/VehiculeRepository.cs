﻿using Dapper;
using Loan.Cars.Model;
using Loan.Cars.Prerequisities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loan.Cars.Repositories
{
    public class VehiculeRepository : BaseRepository
    {
        public VehiculeRepository(System.Data.IDbConnection _con) : base(_con) { } 

        public IEnumerable<Vehicule> GetVehicules(bool carAvailaible , bool isCar = true) 
        {
            var result = Connection.Query<Vehicule, Modele, Marque, CategorieVehicule, Vehicule>("sp_Vehicule_Select",
                            (vehicule, model, brand, cat) =>
                            {
                                model.Marque = brand;
                                model.Categorie = cat;
                                vehicule.Modele = model;
                                return vehicule;
                            }, splitOn: "Id,Id,Id", commandType: CommandType.StoredProcedure, param: new { carAvailaible = carAvailaible, isCar = isCar });
            return result;
        }

        public Vehicule GetVehiculeById(int id)
        {
            var result = Connection.Query<Vehicule, Modele, Marque, CategorieVehicule, Vehicule>("sp_Vehicule_FindById",
                            (vehicule, model, brand, cat) =>
                            {
                                model.Marque = brand;
                                model.Categorie = cat;
                                vehicule.Modele = model;
                                return vehicule;
                            }, splitOn: "Id,Id,Id", commandType: CommandType.StoredProcedure, param: new { Id = id });
            return result.FirstOrDefault();
        }

        public void AddVehicule(Vehicule vehicule)
        {
            ExecuteNonQuery("sp_Vehicule_Add", CommandType.StoredProcedure, new
            {
                 ModeleId = vehicule.Modele?.Id
                ,AgenceId = vehicule.Agence?.Id
                ,Kilometrage = vehicule.Kilometrage
                ,Immatriculation = vehicule.Immatriculation
                ,ChargeMax = vehicule.ChargeMax
                ,ChargeUtile = vehicule.ChargeUtile
                ,DateAchat = vehicule.DateAchat
                ,Climatisation = vehicule.Climatisation
                ,TransmissionType = vehicule.TransmissionType
                ,NombrePlaces = vehicule.NombrePlaces
                ,NombreBagages = vehicule.NombreBagages
            });
        }

        public void UpdateVehicule(Vehicule vehicule)
        {
            ExecuteNonQuery("sp_Vehicule_Add", CommandType.StoredProcedure, new
            {
                Id = vehicule.Id
               ,ModeleId = vehicule.Modele?.Id
               ,AgenceId = vehicule.Agence?.Id
               ,Kilometrage = vehicule.Kilometrage
               ,Immatriculation = vehicule.Immatriculation
               ,ChargeMax = vehicule.ChargeMax
               ,ChargeUtile = vehicule.ChargeUtile
               ,DateAchat = vehicule.DateAchat
               ,Climatisation = vehicule.Climatisation
               ,TransmissionType = vehicule.TransmissionType
               ,NombrePlaces = vehicule.NombrePlaces
               ,NombreBagages = vehicule.NombreBagages
            });
        }

        public void DeleteVehicule(int id)
        {
            ExecuteNonQuery("sp_Vehicule_Delete", CommandType.StoredProcedure, new { Id = id });
        }
    }
}
