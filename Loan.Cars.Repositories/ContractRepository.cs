﻿using Loan.Cars.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loan.Cars.Repositories
{
    public class ContractRepository : Prerequisities.BaseRepository
    {
        public ContractRepository(System.Data.IDbConnection _con) : base(_con) { } 

        public IEnumerable<Contract> GetContracts()
        {
            return null;
        }

        public Contract GetContractById(int id)
        {
            return null;
        }

        public void AddContract(Contract contract)
        {

        }

        public void UpdateContract(Contract contract) 
        {

        }

        public void DeleteContract(int id)
        {

        }
    }
}
