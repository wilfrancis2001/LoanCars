﻿

using System.Collections.Generic;
using System.Data.Common;
using Loan.Cars.Prerequisities;
using System.Data;
using Microsoft.AspNet.Identity;

namespace Loan.Cars.Repositories
{
    public class RoleRepository<TRole> : BaseRepository where TRole : IRole
    {
        public RoleRepository(DbConnection _conn) : base(_conn) { }

        public void Delete(TRole role)
        {
            ExecuteNonQuery( "sp_Role_DeleteById", CommandType.StoredProcedure, new { Id = role.Id } );
        }

        public void Insert(TRole role)
        {
            ExecuteNonQuery("sp_Role_Add", CommandType.StoredProcedure, new { Id = role.Id, Name = role.Name});
        }

        public TRole GetRoleById(string roledId)
        {
            return ExecuteScalar<TRole>("sp_Role_FindById", CommandType.StoredProcedure, new { Id = roledId });
        }

        public TRole GetRoleByName(string roleName)
        {
           return ExecuteScalar<TRole>("sp_Role_FindByName", CommandType.StoredProcedure, new { Name = roleName });
        }

        public bool IsInRole(string roleName)
        {
            return GetRoleByName(roleName) != null;
        }

        public void Update(TRole role)
        {
            ExecuteNonQuery("sp_Role_Update", CommandType.StoredProcedure, new { Id = role.Id, Name = role.Name });
        }

        public IEnumerable<TRole> GetRoles() => ExecuteSelect<TRole>("sp_Role_Select", CommandType.StoredProcedure);
    }
}
