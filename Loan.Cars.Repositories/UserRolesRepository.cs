﻿
using System.Collections.Generic;
using System.Data.Common;
using Dapper;
using Loan.Cars.Prerequisities;
using System.Data;
using System.Linq;
using Loan.Cars.Model;

namespace Loan.Cars.Repositories
{
    public class UserRolesRepository<TUser> : BaseRepository where TUser: class, IDapperIdentity<TUser>
    {
        public UserRolesRepository(DbConnection _con) : base(_con) { }

        public IEnumerable<UserRole> FindByUserId(string userId)
        {
            var result = ExecuteSelect<UserRole>("sp_UserRole_FindById", CommandType.StoredProcedure, new { userId = userId });
            return result;
        }

        public void Delete(string userId)
        {
            ExecuteNonQuery("sp_UserRole_DeleteById", CommandType.StoredProcedure, new { userId = userId });
        }

        public void Insert(string userId, string roleId)
        {
            ExecuteNonQuery("sp_UserRole_Add", CommandType.StoredProcedure, new { userId = userId, roleId = roleId });
        }

        public void Update(string userId, string roleId)
        {
            ExecuteNonQuery("sp_UseRole_Update", CommandType.StoredProcedure, new { userId = userId, roleId = roleId });
        }

        public void RemoveUserFromRole(string userId, string roleName)
        {
            ExecuteNonQuery("sp_UserRole_RemoveUserFromRole", CommandType.StoredProcedure, new { userId = userId, roleName = roleName });
        }
    }
}
