﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using System.Data.Common;
using Dapper;
using Loan.Cars.Prerequisities;
using System.Data;
using Loan.Cars.Model;

namespace Loan.Cars.Repositories
{
    public class UserLoginsRepository<TUser> : BaseRepository where TUser: class, IDapperIdentity<TUser>
    {
        public UserLoginsRepository(IDbConnection _con) : base(_con) { }

        public void Delete(TUser user, UserLoginInfo login)
        {
            ExecuteNonQuery("sp_UserLogin_DeleteByUserId", CommandType.StoredProcedure,
                        new
                        {
                            loginProvider = login.LoginProvider,
                            providerKey = login.ProviderKey,
                            userId = user.Id
                        });
        }

        public void Insert(TUser user, UserLoginInfo login)
        {
            ExecuteNonQuery("sp_UserLogin_Add", CommandType.StoredProcedure,
                        new
                        {
                            loginProvider = login.LoginProvider,
                            providerKey = login.ProviderKey,
                            userId = user.Id
                        });
        }

        public string FindUserIdbyLogin(UserLoginInfo login)
        {
            var userLogin = ExecuteScalar<UserLogin>("sp_UserLogin_FindByLogin", CommandType.StoredProcedure, new {
                loginProvider = login.LoginProvider,
                providerKey = login.ProviderKey
            });
            return userLogin?.UserId;
        }

        public IEnumerable<UserLoginInfo> FindAllByUserId(string userId)
        {
            var userLogins = ExecuteSelect<UserLogin>("sp_UserLogin_FindByUserId", CommandType.StoredProcedure, new { userId = userId });
            return userLogins.Select(x => new UserLoginInfo(x.LoginProvider, x.ProviderKey));
        }
    }
}
