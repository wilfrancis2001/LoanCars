﻿using Loan.Cars.Model;
using Loan.Cars.Prerequisities;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;

namespace Loan.Cars.Repositories
{
    public class ModeleVehiculeRepository : BaseRepository
    {
        public ModeleVehiculeRepository(IDbConnection _con) : base(_con) { } 

        public IEnumerable<Modele> GetModeleVehicules() 
        {
            IEnumerable<Modele> result = null;
            if (EnsureConnectionOpen())
            {
                result = Connection.Query<Modele,Marque, CategorieVehicule, Modele>("sp_ModeleVehicule_Select", (model, brand, cat) =>
                {
                    model.Categorie = cat;
                    model.Marque = brand;
                    return model;
                }, splitOn: "Id,Id", commandType: CommandType.StoredProcedure);
            }
            return result;
        }

        public Modele GetModeleVehiculeByCode(string code)
        {
            Modele result = null;
            if (EnsureConnectionOpen())
            {
                result = Connection.Query<Modele, Marque, CategorieVehicule, Modele>("sp_ModeleVehicule_FindByCode", (model, brand, cat) =>
                {
                    model.Categorie = cat;
                    model.Marque = brand;
                    return model; 
                },splitOn : "Id,Id", param: new { code = code }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }

        public IEnumerable<Modele> GetModeleVehiculeByMarqueId(int marqueId)  
        {
            IEnumerable<Modele> result = null;
            if (EnsureConnectionOpen())
            {
                result = Connection.Query<Modele, Marque, CategorieVehicule, Modele>("sp_ModeleVehicule_FindByMarqueId", (model, brand, cat) =>
                {
                    model.Categorie = cat;
                    model.Marque = brand;
                    return model;
                }, splitOn: "Id,Id", param:new { marqueId = marqueId }, commandType: CommandType.StoredProcedure);
            }
            return result;
        }

        public bool IsInModele(string code)
        {
            return GetModeleVehiculeByCode(code) != null;
        }

        public void AddModeleVehicule(Modele modele)
        {
            ExecuteNonQuery("sp_ModeleVehicule_Add", CommandType.StoredProcedure, new
            {
                name = modele.Name,
                code = modele.Code,
                marqueId = modele.Marque.Id,
                vehiculecategorieId = modele.Categorie.Id
            });
        }
    }
}
