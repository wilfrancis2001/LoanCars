﻿


----- 		deleting of foreign keys		-----
-------------------------------------------------

if  exists (select * from sys.foreign_keys where object_id = OBJECT_ID(N'[dbo].[FK_AspNetUserClaims]'))
alter table [dbo].[AspNetUserClaims] drop constraint [FK_AspNetUserClaims]
go

if  exists (select * from sys.foreign_keys where object_id = OBJECT_ID(N'[dbo].[FK_AspNetUsers]'))
alter table [dbo].[AspNetUsers] drop constraint [FK_AspNetUsers]
go

if  exists (select * from sys.foreign_keys where object_id = OBJECT_ID(N'[dbo].[FK_AspNetUserLogins]'))
alter table [dbo].[AspNetUserLogins] drop constraint [FK_AspNetUserLogins]
go

if  exists (select * from sys.foreign_keys where object_id = OBJECT_ID(N'[dbo].[FK_AspNetUserRole_Role]'))
alter table [dbo].[AspNetUserRole] drop constraint [FK_AspNetUserRole_Role]
go

if  exists (select * from sys.foreign_keys where object_id = OBJECT_ID(N'[dbo].[FK_AspNetUserRole_User]'))
alter table [dbo].[AspNetUserRole] drop constraint [FK_AspNetUserRole_User]
go

if  exists (select * from sys.foreign_keys where object_id = OBJECT_ID(N'[dbo].[FK_Modele_MarqueId]'))
alter table [dbo].[Modele] drop constraint FK_Modele_MarqueId
go

if  exists (select * from sys.foreign_keys where object_id = OBJECT_ID(N'[dbo].[FK_Modele_VehiculeCategorieId]'))
alter table [dbo].[Modele] drop constraint FK_Modele_VehiculeCategorieId
go

if  exists (select * from sys.foreign_keys where object_id = OBJECT_ID(N'[dbo].[FK_Vehicule_AgenceId]'))
alter table [dbo].[Vehicule] drop constraint FK_Vehicule_AgenceId
go

if  exists (select * from sys.foreign_keys where object_id = OBJECT_ID(N'[dbo].[FK_Vehicule_ModeleId]'))
alter table [dbo].[Vehicule] drop constraint FK_Vehicule_ModeleId
go

if  exists (select * from sys.foreign_keys where object_id = OBJECT_ID(N'[dbo].[FK_Booking_UserId]'))
alter table [dbo].[Booking] drop constraint FK_Booking_UserId
go

if  exists (select * from sys.foreign_keys where object_id = OBJECT_ID(N'[dbo].[FK_Booking_VehiculeId]'))
alter table [dbo].[Booking] drop constraint FK_Booking_VehiculeId
go


------	 	 deleting of tables		  --------
----------------------------------------------

if exists (select * from dbo.sysobjects where id = OBJECT_ID(N'[dbo].[AspNetUserClaims]'))
drop table [dbo].[AspNetUserClaims]
go

if exists (select * from dbo.sysobjects where id = OBJECT_ID(N'[dbo].[AspNetUserLogins]'))
drop table [dbo].[AspNetUserLogins]
go

if exists (select * from dbo.sysobjects where id = OBJECT_ID(N'[dbo].[AspNetUserRole]'))
drop table [dbo].[AspNetUserRole]
go

if exists (select * from dbo.sysobjects where id = OBJECT_ID(N'[dbo].[AspNetRole]'))
drop table [dbo].[AspNetRole]
go

if exists (select * from dbo.sysobjects where id = OBJECT_ID(N'[dbo].[AspNetUsers]'))
drop table [dbo].[AspNetUsers]
go 

if exists (select * from dbo.sysobjects where id = OBJECT_ID(N'[dbo].[Modele]'))
drop table [dbo].[Modele]
go

if exists (select * from dbo.sysobjects where id = OBJECT_ID(N'[dbo].[Marque]'))
drop table [dbo].[Marque]
go

if exists (select * from dbo.sysobjects where id = OBJECT_ID(N'[dbo].[Agence]'))
drop table [dbo].[Agence]
go

if exists (select * from dbo.sysobjects where id = OBJECT_ID(N'[dbo].[Booking]'))
drop table [dbo].[Booking]
go

if exists (select * from dbo.sysobjects where id = OBJECT_ID(N'[dbo].[Vehicule]'))
drop table [dbo].[Vehicule]
go

if exists (select * from dbo.sysobjects where id = OBJECT_ID(N'[dbo].[VehiculeCategorie]'))
drop table [dbo].[VehiculeCategorie]
go



create table Agence
(
	Id int identity(1,1) NOT NULL,
	Name varchar(100) NOt NULL,
	StreetNumber int NULL,
	PhoneNumber varchar(50) NOT NULL,
	StreetName varchar(150) NOT NULL,
	AdditionAdress varchar(150) NULL,
	ZipCode char(5) NOT NULL,
	Town varchar(100) NOT NULL,
	Country varchar(150) NOT NULL,
	constraint PK_Agence_Id primary key(Id)
);


------	 		creating of tables	     	------
--------------------------------------------------

CREATE TABLE AspNetUsers 
(
  Id varchar(128) NOT NULL,
  AgenceId int NULL,
  Email varchar(256) NOT NULL,
  UserName varchar(256) NOT NULL,
  FirstName nvarchar(256) NULL ,
  LastName nvarchar(256) NULL,
  AccessFailedCount int NULL DEFAULT(0),
  IsActive bit NULL DEFAULT(0),
  IsDelete bit NULL DEFAULT(0),
  LockoutEnabled bit NULL DEFAULT(0),
  EmailConfirmed bit NULL DEFAULT(0),
  TwoFactorEnabled bit NULL DEFAULT(0),
  PhoneNumberConfirmed bit NULL DEFAULT(0),
  PhoneNumber varchar(256) NOT NULL,
  StreetNumber int NULL,
  StreetName varchar(256) NOT NULL,
  AdditionnalInfo varchar(256) NULL,
  BoxOffice int NOT NULL,
  Town varchar(256) NOT NULL,
  Country varchar(128) NOT NULL,
  PasswordHash varchar(256) NOT NULL,
  SecurityStamp varchar(256) NOT NULL,
  LockoutEndDateUtc datetimeoffset DEFAULT NULL,
  CreationDate datetimeoffset(2) NOT NULL DEFAULT(GETDATE()), 
  UpdateDate datetimeoffset(2) NULL, 
  CONSTRAINT PK_AspNetUsers PRIMARY KEY (Id),
  CONSTRAINT FK_AspNetUsers FOREIGN KEY (AgenceId) REFERENCES Agence(Id) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT UK_AspNetUsers_Email UNIQUE(Email),
  CONSTRAINT UK_AspNetUsers_UserName UNIQUE(UserName)
);

CREATE TABLE AspNetRole 
(
  Id varchar(128) NOT NULL,
  Name varchar(256) NOT NULL,
  CONSTRAINT PK_AspNetRole PRIMARY KEY (Id),
  CONSTRAINT UK_AspNetRole_Name UNIQUE(Name)
);

CREATE TABLE AspNetUserClaims 
(
  Id int identity(1,1) NOT NULL,
  UserId varchar(128) NOT NULL,
  ClaimType varchar(256), 
  ClaimValue varchar(256),
  CONSTRAINT PK_AspNetUserClaims PRIMARY KEY (Id),
  CONSTRAINT UK_AspNetUserClaims UNIQUE (UserId,ClaimType),
  CONSTRAINT FK_AspNetUserClaims FOREIGN KEY (UserId) REFERENCES AspNetUsers(Id) ON DELETE CASCADE ON UPDATE NO ACTION
);

CREATE TABLE AspNetUserLogins 
(
  LoginProvider varchar(128) NOT NULL,
  ProviderKey varchar(128) NOT NULL,
  UserId varchar(128) NOT NULL,
  CONSTRAINT PK_AspNetUserLogins PRIMARY KEY (LoginProvider,ProviderKey,UserId),
  CONSTRAINT UK_AspNetUserLogins UNIQUE (UserId),
  CONSTRAINT FK_AspNetUserLogins FOREIGN KEY (UserId) REFERENCES AspNetUsers(Id) ON DELETE CASCADE ON UPDATE NO ACTION
);

CREATE TABLE AspNetUserRole 
(
  UserId varchar(128) NOT NULL,
  RoleId varchar(128) NOT NULL
  CONSTRAINT PK_AspNetUserRole PRIMARY KEY (UserId,RoleId),
  --CONSTRAINT UK_AspNetUserRole UNIQUE(UserId),
  CONSTRAINT FK_AspNetUserRole_User FOREIGN KEY (UserId) REFERENCES AspNetUsers (Id) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT FK_AspNetUserRole_Role FOREIGN KEY (RoleId) REFERENCES AspNetRole (Id) ON DELETE CASCADE ON UPDATE NO ACTION
);

create table VehiculeCategorie
(
	Id int identity(1,1) NOT NULL,
	Name varchar(50) NOT NULL,
	Code varchar(50) NOT NULL,
	constraint PK_VehiculeCategorie_Id primary key (Id),
	constraint UK_VehiculeCategorie_Code unique(Code)
);

create table Marque 
(
	Id int identity(1,1) NOT NULL,
	Name varchar(100) NOT NULL,
	Code varchar(50) NOT NULL,
	constraint UK_Marque_Name unique(Name),
	constraint PK_Marque_Id primary key clustered(Id)
);

create table Modele 
(
	Id int identity(1,1) NOT NULL,
	MarqueId int NOT NULL,
	VehiculeCategorieId int NOT NULL,
	Code varchar(50) NOT NULL,
	Name varchar(100) NOT NULL,
	constraint UK_Modele_Name unique(Name),
	constraint PK_Modele_Id primary key clustered(Id),
	constraint FK_Modele_MarqueId foreign key(MarqueId) references Marque(Id),
	constraint FK_Modele_VehiculeCategorieId foreign key(VehiculeCategorieId) references VehiculeCategorie(Id)
);

create table Vehicule
(
	Id int identity(1,1) NOT NULL,
	ModeleId int NOT NULL,
	AgenceId int NOT NULL,
	Kilometrage int NOT NULL,
	Immatriculation varchar(15) NOT NULL,
	ChargeMax int NULL,
	ChargeUtile int NULL,
	DateAchat date NOT NULL,
	Climatisation bit NULL default(0),
	TransmissionType varchar(50) NOT NULL,
	NombrePlaces int NULL,
	NombreBagages int NULL,
	constraint PK_Vehicule_Id primary key(Id),
	constraint UK_Vehicule_Immatriculation unique(Immatriculation),
	constraint FK_Vehicule_AgenceId foreign key(AgenceId) references Agence(Id),
	constraint FK_Vehicule_ModeleId foreign key(ModeleId) references Modele(Id)
);

create table Booking
(
	Id int identity(1,1) NOT NULL,
	UserId varchar(128) NOT NULL,
	VehiculeId int NOT NULL,
	Code varchar(20) NOT NULL,
	Status int NOT NULL,
	StartDate datetime NOT NULL,
	EndDate datetime NOT NULL,
	KmStart int NOT NULL,
	KmEnd int NOT NULL,
	Accompte decimal NULL,
	CreateDate datetime NOT NULL,
	UpdateDate datetime NULL,
	constraint UK_Booking_Code unique(Code),
	constraint PK_Booking_User_Vehicule_Id primary key(Id),
	constraint CK_Booking_Date_Km check(KmEnd > KmStart and EndDate > StartDate),
	constraint FK_Booking_UserId foreign key(UserId) references AspNetUsers(Id),
	constraint FK_Booking_VehiculeId foreign key(VehiculeId) references Vehicule(Id)
);


------	 		creating indexes	     	------
--------------------------------------------------

if not exists (select * from sysindexes where name='IX_AspNetUserLogins')
	CREATE UNIQUE INDEX IX_AspNetUserLogins ON AspNetUserLogins(UserId);  

if not exists (select * from sysindexes where name='IX_AspNetUserClaims')
	CREATE UNIQUE INDEX IX_AspNetUserClaims ON AspNetUserClaims(UserId); 

--if not exists (select * from sysindexes where name='IX_AspNetUserRole')
--	CREATE UNIQUE INDEX IX_AspNetUserRole ON AspNetUserRole(RoleId);  
