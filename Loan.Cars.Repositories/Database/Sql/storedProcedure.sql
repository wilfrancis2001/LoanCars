
-----------------------------------------------
--	  drop existing stored procedures		 --
-----------------------------------------------

if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_User_Select')				    drop procedure sp_User_Select; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_User_Add')					    drop procedure sp_User_Add; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_User_Update')				    drop procedure sp_User_Update; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_User_FindById')			    drop procedure sp_User_FindById; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_User_FindByName')			    drop procedure sp_User_FindByName; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_User_FindByEmail')			    drop procedure sp_User_FindByEmail; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_User_DeleteById')			    drop procedure sp_User_DeleteById;
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_employees_Get')			    drop procedure sp_employees_Get; 
																								    
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_Role_Select')				    drop procedure sp_Role_Select; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_Role_Add')					    drop procedure sp_Role_Add; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_Role_FindByName')			    drop procedure sp_Role_FindByName;
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_Role_Update')				    drop procedure sp_Role_Update; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_Role_FindById')			    drop procedure sp_Role_FindById; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_Role_DeleteById')			    drop procedure sp_Role_DeleteById; 

if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_UseRole_Update')				drop procedure sp_UseRole_Update; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_UserRole_Add')					drop procedure sp_UserRole_Add; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_UserRole_FindById')			drop procedure sp_UserRole_FindById; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_UserRole_DeleteById')			drop procedure sp_UserRole_DeleteById; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_UserRole_RemoveUserFromRole')	drop procedure sp_UserRole_RemoveUserFromRole; 

if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_UserClaim_Add')			    drop procedure sp_UserClaim_Add; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_UserClaim_FindByUserId')	    drop procedure sp_UserClaim_FindByUserId; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_UserClaim_DeleteByUserId')	    drop procedure sp_UserClaim_DeleteByUserId; 
																								    
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_UserLogin_Add')			    drop procedure sp_UserLogin_Add; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_UserLogin_FindByLogin')	    drop procedure sp_UserLogin_FindByLogin; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_UserLogin_DeleteByUserId')	    drop procedure sp_UserLogin_DeleteByUserId; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_UserLogin_FindByUserId')	    drop procedure sp_UserLogin_FindByUserId; 

if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_Agence_Select')			    drop procedure sp_Agence_Select; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_Agence_Add')					drop procedure sp_Agence_Add; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_Agence_FindById')			    drop procedure sp_Agence_FindById; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_Agence_Update')			    drop procedure sp_Agence_Update; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_Agence_DeleteById')			drop procedure sp_Agence_DeleteById; 


if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_Booking_Select')				drop procedure sp_Booking_Select; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_Booking_Add')					drop procedure sp_Booking_Add; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_Booking_Update')				drop procedure sp_Booking_Update; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_Booking_FindById')				drop procedure sp_Booking_FindById; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_Booking_FindByCode')			drop procedure sp_Booking_FindByCode; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_Booking_DeleteById')			drop procedure sp_Booking_DeleteById; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_Booking_DeleteByCode')			drop procedure sp_Booking_DeleteByCode; 


if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_Vehicule_Select')				drop procedure sp_Vehicule_Select; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_Vehicule_FindById')			drop procedure sp_Vehicule_FindById; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_Vehicule_Add')					drop procedure sp_Vehicule_Add; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_Vehicule_Update')				drop procedure sp_Vehicule_Update; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_Vehicule_Delete')				drop procedure sp_Vehicule_Delete; 


if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_CategorieVehicule_Select')		drop procedure sp_CategorieVehicule_Select; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_CategorieVehicule_Add')		drop procedure sp_CategorieVehicule_Add; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_CategorieVehicule_FindByCode')	drop procedure sp_CategorieVehicule_FindByCode; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_CategorieVehicule_FindByModelId')		drop procedure sp_CategorieVehicule_FindByModelId; 


if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_MarqueVehicule_Select')		drop procedure sp_MarqueVehicule_Select; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_MarqueVehicule_Add')			drop procedure sp_MarqueVehicule_Add; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_MarqueVehicule_FindByCode')	drop procedure sp_MarqueVehicule_FindByCode; 


if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_ModeleVehicule_Select')		drop procedure sp_ModeleVehicule_Select; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_ModeleVehicule_Add')			drop procedure sp_ModeleVehicule_Add; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_ModeleVehicule_FindByMarqueId')drop procedure sp_ModeleVehicule_FindByMarqueId; 
if exists(select * from sys.procedures WITH(NOLOCK) where name = 'sp_ModeleVehicule_FindByCode')	drop procedure sp_ModeleVehicule_FindByCode; 
go


--------------------------------------------
--			User identity				  --
--------------------------------------------

create procedure sp_User_Select
as 
	select * from aspnetusers
go

create procedure sp_User_Add
	@Id nvarchar(128),
	@AgenceId int,
	@Email nvarchar(256),
	@UserName nvarchar(256),
	@FirstName nvarchar(256),
	@LastName nvarchar(256),
	@PhoneNumber nvarchar(256),
	@StreetNumber int,
	@StreetName varchar(256),
	@AdditionnalInfo varchar(256),
	@BoxOffice int,
	@Town varchar(256),
	@Country varchar(128),
	@PasswordHash nvarchar(256),
	@SecurityStamp nvarchar(256),
	@CreationDate datetimeoffset(2),
	@LockoutEndDateUtc datetimeoffset(2) = null
as
begin
	insert 
		into AspNetUsers
			(
				Id,
				AgenceId,
				Email,
				UserName,
				FirstName,
				LastName,
				PhoneNumber,
				StreetNumber,
				StreetName,
				AdditionnalInfo,
				BoxOffice,
				Town,
				Country,
				PasswordHash,
				SecurityStamp,
				LockoutEndDateUtc,
				CreationDate
			) 
			values
			(
				@Id,
				@AgenceId,
				@Email,
				@UserName,
				@FirstName,
				@LastName,
				@PhoneNumber,
				@StreetNumber,
				@StreetName,
				@AdditionnalInfo,
				@BoxOffice,
				@Town,
				@Country,
				@PasswordHash,							 
				@SecurityStamp,
				@LockoutEndDateUtc,
				@CreationDate
			)

  

    select scope_identity()
end;  
go

create procedure sp_User_Update
	@Id nvarchar(128),
	@AgenceId int,
	@Email nvarchar(256),
	@UserName nvarchar(256),
	@FirstName nvarchar(256),
	@LastName nvarchar(256),
	@IsActive bit,
	@EmailConfirmed bit,
	@AccessFailedCount int,
	@PhoneNumber nvarchar(256),
	@StreetNumber int,
	@StreetName varchar(256),
	@AdditionnalInfo varchar(256),
	@BoxOffice int,
	@Town varchar(256),
	@Country varchar(128),
	@PasswordHash nvarchar(256),
	@SecurityStamp nvarchar(256),
	@UpdateDate datetimeoffset(2)
as
	update AspNetUsers
	   set Email = @Email,
	       AgenceId  = @AgenceId,
	  	   UserName = @UserName,
		   FirstName = @FirstName,
		   LastName = @LastName,
		   IsActive = @IsActive,
		   EmailConfirmed = @EmailConfirmed,
		   AccessFailedCount = @AccessFailedCount,
	  	   PhoneNumber = @PhoneNumber,
		   StreetNumber = @StreetNumber,
		   StreetName = @StreetName,
		   AdditionnalInfo = @AdditionnalInfo,
		   BoxOffice = @BoxOffice,
		   Town = @Town,
		   Country = @Country,
	  	   PasswordHash = @PasswordHash,
	  	   SecurityStamp = @SecurityStamp,
	  	   UpdateDate = @UpdateDate
	   where Id = @Id
go

create procedure sp_User_FindById
	@Id nvarchar(128)
as 
	select * from AspNetUsers where Id = @Id
go

create procedure sp_User_FindByName
	@userName nvarchar(128)
as 
	select * from AspNetUsers where UserName = @userName
go

create procedure sp_User_FindByEmail
	@email nvarchar(128)
as 
	select * from AspNetUsers where Email = @email
go

create procedure sp_User_DeleteById
	@Id nvarchar(128)
as 
	--update AspNetUsers
	--   set IsActive = 0,
	--	   EmailConfirmed = 0,
	--	   PhoneNumberConfirmed = 0,
	--	   IsDelete = 1,
	--	   LockoutEnabled = 1,
	--	   UpdateDate = getdate()
	--   where Id = @Id
	delete from AspNetUsers where Id = @Id 
go


create procedure sp_employees_Get
as
	select au.* 
	from aspnetusers au
	inner join aspnetuserrole aur on au.id = aur.userId
	inner join aspnetrole ar on ar.id = aur.roleid
	where ar.name != 'BASIC'
go 


--------------------------------------------
--			  Role identity				  --		
--------------------------------------------

create procedure sp_Role_Select
as
begin
	--select anr.Id,
	--	   anr.Name,
	--	   anur.UserId,
	--	   anu.* from AspNetRole anr
	--left join AspNetUserRole anur on anur.RoleId = anr.Id
	--left join AspNetUsers anu on anur.UserId = anu.Id

	select * from AspNetRole
end
go

create procedure sp_Role_Add
    @Id nvarchar(128),
	@Name nvarchar(128)
as
begin
	insert 
		into AspNetRole 
		(
			Id,
			Name
		)
		values
		(
			@Id,
			@Name
		)
	select scope_identity()
end
go

create procedure sp_Role_Update
	@Id nvarchar(128),
	@Name nvarchar(128)
as
begin
	update AspNetRole set Name = @Name where Id = @Id;
end
go

create procedure sp_Role_FindById
	@Id nvarchar(128)
as
	select * from AspNetRole where Id = @Id
go

create procedure sp_Role_FindByName
	@Name nvarchar(128)
as
	select * from AspNetRole where Name = @Name
go

create procedure sp_Role_DeleteById
	@Id nvarchar(128)
as
	delete from AspNetRole where Id = @Id
go

--------------------------------------------
--			User Role identity			  --	
--------------------------------------------
create procedure sp_UserRole_Add
	@userId nvarchar(128),
	@roleId nvarchar(128)
as
begin
	insert 
		into AspNetUserRole
		(
			UserId,
			RoleId
		)
		values
		(
			@userId,
			@roleId
		)
	select scope_identity()
end
go

create procedure sp_UserRole_FindById
	@userId nvarchar(128)
as
	select aur.*, ar.Name as RoleName from AspNetUserRole aur
	inner join AspNetRole ar on ar.Id = aur.RoleId
	where aur.UserId = @userId
go

create procedure sp_UserRole_DeleteById
	@userId nvarchar(128)
as
	delete from AspNetUserRole where UserId = @userId
go

create procedure sp_UserRole_RemoveUserFromRole
	@userId nvarchar(128),
	@roleName nvarchar(128)
as
	delete u from AspNetUserRole u
    inner join AspNetRole r on u.RoleId = r.Id
    where r.Name = @roleName and u.userid = @userId
go

create procedure sp_UseRole_Update
	@userId nvarchar(128),
	@roleId nvarchar(128)
as
begin
	exec sp_UserRole_DeleteById @userId;
	exec sp_UserRole_Add @userId, @roleId;
end
go

--------------------------------------------
--		User Claim identity				  --
--------------------------------------------
create procedure sp_UserClaim_Add
	@userId nvarchar(128),
	@claimValue varchar(256),
	@claimType varchar(256)
as 
begin 
	insert 
		into AspNetUserClaims 
		(
			UserId,
			ClaimType, 
			ClaimValue
	    ) 
		values	
		(
			@UserId, 
			@claimType, 
			@claimValue
		) 
	select scope_identity()
end
go

create procedure sp_UserClaim_FindByUserId
	@userId nvarchar(128)
as 
	select * from AspNetUserClaims where UserId = @userId 
go

create procedure sp_UserClaim_DeleteByUserId
	@userId nvarchar(128),
	@claimValue varchar(256),
	@claimType varchar(256)
as 
	delete from AspNetUserClaims where UserId = @userId and ClaimType = @claimType and ClaimValue = @claimValue
go

-------------------------------------------
--		User Login identity	     		 --
-------------------------------------------
create procedure sp_UserLogin_Add
	@userId nvarchar(128),
	@providerKey varchar(256),
	@loginProvider varchar(256)
as 
begin 
	insert 
		into AspNetUserLogins 
		(
			UserId,
			ProviderKey, 
			LoginProvider
	    ) 
		values	
		(
			@userId, 
			@providerKey, 
			@loginProvider
		) 
	select scope_identity()
end
go

create procedure sp_UserLogin_FindByUserId
	@userId nvarchar(128)
as 
	select * from AspNetUserLogins where UserId = @userId
go

create procedure sp_UserLogin_FindByLogin
	@providerKey varchar(256),
	@loginProvider varchar(256)
as 
	select * from AspNetUserLogins where ProviderKey = @providerKey and LoginProvider = @loginProvider
go

create procedure sp_UserLogin_DeleteByUserId
	@userId nvarchar(128),
	@providerKey varchar(256),
	@loginProvider varchar(256)
as 
	delete from AspNetUserLogins where UserId = @userId and ProviderKey = @providerKey and LoginProvider = @loginProvider
go


-------------------------------------------
--				Agence		     		 --
-------------------------------------------

create procedure sp_Agence_Select
as 
	select * from Agence
go

create procedure sp_Agence_Add
	@name nvarchar(128),
	@phoneNumber varchar(20),
	@streetNumber int,
	@streetName varchar(256),
	@additioAdress varchar(256),
	@zipCode int,
	@town varchar(128),
	@country varchar(128)
as 
begin 
	insert 
		into Agence 
		(
			Name,
			PhoneNumber,
			StreetNumber,
			StreetName, 
			AdditionAdress,
			ZipCode,
			Town,
			Country
	    ) 
		values	
		(
			@name, 
			@phoneNumber,
			@streetNumber, 
			@streetName,
			@additioAdress,
			@zipCode,
			@town,
			@country
		) 
	select scope_identity()
end
go

create procedure sp_Agence_FindById
	@id int
as 
	select * from Agence where Id = @id
go

create procedure sp_Agence_Update
	@id int,
	@name nvarchar(128),
	@phoneNumber varchar(20),
	@streetNumber int,
	@streetName varchar(256),
	@additioAdress varchar(256),
	@zipCode int,
	@town varchar(128),
	@country varchar(128)
as 
	update Agence
	set Name = @name,
	    PhoneNumber = @phoneNumber,
		StreetNumber = @streetNumber,
		StreetName = @streetName, 
		AdditionAdress = @additioAdress,
		ZipCode = @zipCode,
		Town = @town,
		Country = @country 
	where Id = @id
go

create procedure sp_Agence_DeleteById
	@id int
as 
	delete from Agence where Id = @id
go

-------------------------------------------
--				Vehicule	     		 --
-------------------------------------------

create procedure sp_Vehicule_Select
	@carAvailaible bit,
	@isCar bit
as
BEGIN
	if @carAvailaible = 1
		begin
			if @isCar = 1
				begin
					select v.*
						  ,m.*
						  ,ma.*
						  ,vc.*
					from vehicule v
					inner join modele m on m.Id = v.ModeleId
					inner join marque ma on ma.Id = m.MarqueId
					inner join VehiculeCategorie vc on vc.Id = m.VehiculeCategorieId
					left join Booking b on b.VehiculeId = v.Id
					where vc.Code != 'UTILITAIRE' and v.Id is null
				end
			else
				begin
					select v.*
						  ,m.*
						  ,ma.*
						  ,vc.*
					from vehicule v
					inner join modele m on m.Id = v.ModeleId
					inner join marque ma on ma.Id = m.MarqueId
					inner join VehiculeCategorie vc on vc.Id = m.VehiculeCategorieId
					left join Booking b on b.VehiculeId = v.Id
					where vc.Code = 'UTILITAIRE' and v.Id is null
				end
		end
	else
		begin
			if @isCar = 1
				begin
					select v.*
						  ,m.*
						  ,ma.*
						  ,vc.*
					from vehicule v
					inner join modele m on m.Id = v.ModeleId
					inner join marque ma on ma.Id = m.MarqueId
					inner join VehiculeCategorie vc on vc.Id = m.VehiculeCategorieId
					where vc.Code != 'UTILITAIRE'
				end
			else
				begin
					select v.*
						  ,m.*
						  ,ma.*
						  ,vc.*
					from vehicule v
					inner join modele m on m.Id = v.ModeleId
					inner join marque ma on ma.Id = m.MarqueId
					inner join VehiculeCategorie vc on vc.Id = m.VehiculeCategorieId
					where vc.Code = 'UTILITAIRE'
				end
		end
	--select * from Vehicule where Id not in (select distinct vehiculeId from Booking where EndDate > getdate())
END 
GO

create procedure sp_Vehicule_FindById
	@Id int
as
	select v.*
		  ,m.*
		  ,ma.*
		  ,vc.*
	from vehicule v
	inner join modele m on m.Id = v.ModeleId
	inner join marque ma on ma.Id = m.MarqueId
	inner join VehiculeCategorie vc on vc.Id = m.VehiculeCategorieId
	where v.Id != @Id
GO

create procedure sp_Vehicule_Add
	 @ModeleId int
	,@AgenceId int
	,@Kilometrage int
	,@Immatriculation varchar(15)
	,@ChargeMax int
	,@ChargeUtile int
	,@DateAchat date
	,@Climatisation bit
	,@TransmissionType varchar(50)
	,@NombrePlaces int
	,@NombreBagages int
as
BEGIN
	insert into Vehicule
	(
		 ModeleId 
		,AgenceId
		,Kilometrage 
		,Immatriculation 
		,ChargeMax 
		,ChargeUtile 
		,DateAchat 
		,Climatisation 
		,TransmissionType 
		,NombrePlaces 
		,NombreBagages
	)
	values
	(
		 @ModeleId 
		,@AgenceId
		,@Kilometrage 
		,@Immatriculation 
		,@ChargeMax 
		,@ChargeUtile 
		,@DateAchat 
		,@Climatisation 
		,@TransmissionType 
		,@NombrePlaces 
		,@NombreBagages 
	)
END
GO

create procedure sp_Vehicule_Update
	 @Id int 
	,@ModeleId int
	,@AgenceId int
	,@Kilometrage int
	,@Immatriculation varchar(15)
	,@ChargeMax int
	,@ChargeUtile int
	,@DateAchat date
	,@Climatisation bit
	,@TransmissionType varchar(50)
	,@NombrePlaces int
	,@NombreBagages int
as
BEGIN
	update Vehicule 
	set  ModeleId = @ModeleId
		,AgenceId = @AgenceId
		,Kilometrage = @Kilometrage
		,Immatriculation = @Immatriculation
		,ChargeMax = @ChargeMax
		,ChargeUtile = @ChargeUtile
		,DateAchat = @DateAchat
		,Climatisation = @Climatisation
		,TransmissionType = @TransmissionType
		,NombrePlaces = @NombrePlaces
		,NombreBagages = @NombreBagages
	where Id = @Id
END
GO

create procedure sp_Vehicule_Delete
	@Id int
as
 delete from Vehicule where Id = @Id
GO


-------------------------------------------
--		categorie Vehicule	     		 --
-------------------------------------------

create procedure sp_CategorieVehicule_Select
as
	select * from VehiculeCategorie
go

create procedure sp_CategorieVehicule_Add
	@name varchar(50),
	@code varchar(50)
as
	insert into 
	VehiculeCategorie
	(
		Name,
		Code
	)
	values
	(
		@name,
		@code
	)
go

create procedure sp_CategorieVehicule_FindByCode
	@code varchar(50)
as
	select * from VehiculeCategorie where Code = @code
go

create procedure sp_CategorieVehicule_FindByModelId
	@modelId int
as
	select distinct * from VehiculeCategorie vc
	inner join Modele m on m.VehiculeCategorieId = vc.Id
	where vc.Id = @modelId
	order by vc.Id
go


-------------------------------------------
--			marque Vehicule	     		 --
-------------------------------------------

create procedure sp_MarqueVehicule_Select
as
	select * from Marque
go

create procedure sp_MarqueVehicule_Add
	@name varchar(50),
	@code varchar(50)
as
	insert into 
	Marque
	(
		Name,
		Code
	)
	values
	(
		@name,
		@code
	)
go

create procedure sp_MarqueVehicule_FindByCode
	@code varchar(50)
as
	select * from Marque where Code = @code
go

-------------------------------------------
--			Modele Vehicule	     		 --
-------------------------------------------

create procedure sp_ModeleVehicule_Select
as
	select mo.Id
		  ,mo.Code
		  ,mo.Name 
		  ,ma.Id
		  ,ma.Code
		  ,ma.Name
		  ,vc.Id
		  ,vc.Code
		  ,vc.Name
	from Modele mo
	inner join Marque ma on mo.marqueId = ma.Id
	inner join VehiculeCategorie vc on mo.VehiculeCategorieId = vc.Id
	order by ma.Id
go

create procedure sp_ModeleVehicule_Add
	@name varchar(50),
	@code varchar(50),
	@marqueId int,
	@vehiculecategorieId int
as
	insert into 
	Modele
	(
		 Code
		,Name
		,VehiculeCategorieId
		,MarqueId
	)
	values
	(
		 @code
		,@name
		,@vehiculecategorieId
		,@marqueId
	)
go

create procedure sp_ModeleVehicule_FindByMarqueId
	@marqueId varchar(50)
as
	select mo.Id
		  ,mo.Code
		  ,mo.Name 
		  ,ma.Id
		  ,ma.Code
		  ,ma.Name
		  ,vc.Id
		  ,vc.Code
		  ,vc.Name
	from Modele mo
	inner join Marque ma on mo.marqueId = ma.Id
	inner join VehiculeCategorie vc on mo.VehiculeCategorieId = vc.Id
	where ma.Id = @marqueId
	order by ma.Id
go

create procedure sp_ModeleVehicule_FindByCode
	@code varchar(50)
as
	select mo.Id
		  ,mo.Code
		  ,mo.Name 
		  ,ma.Id
		  ,ma.Code
		  ,ma.Name
		  ,vc.Id
		  ,vc.Code
		  ,vc.Name
	from Modele mo
	inner join Marque ma on mo.marqueId = ma.Id
	inner join VehiculeCategorie vc on mo.VehiculeCategorieId = vc.Id
	where mo.Code = @code
	order by ma.Id
go


-------------------------------------------
--				Booking		     		 --
-------------------------------------------

create procedure sp_Booking_Select
	@userId varchar(128)
as
begin
	if @userId is null
		select b.* 
			  ,v.*
			  ,m.*
			  ,ma.*
			  ,vc.*
		from Booking b
		inner join Vehicule v on v.Id = b.VehiculeId
		inner join Modele m on m.Id = v.ModeleId
		inner join Marque ma on ma.Id = m.MarqueId
		inner join VehiculeCategorie vc on vc.Id = m.VehiculeCategorieId
	else
		select b.* 
			  ,v.*
			  ,m.*
			  ,ma.*
			  ,vc.*
		from Booking b
		inner join Vehicule v on v.Id = b.VehiculeId
		inner join Modele m on m.Id = v.ModeleId
		inner join Marque ma on ma.Id = m.MarqueId
		inner join VehiculeCategorie vc on vc.Id = m.VehiculeCategorieId
		where b.UserId = @userId
end
go

create procedure sp_Booking_Add
	@UserId varchar(128)
   ,@VehiculeId int
   ,@Code varchar(128)
   ,@Status int
   ,@StartDate datetime
   ,@EndDate datetime
   ,@KmStart int
   ,@KmEnd int
   ,@Accompte decimal
   ,@CreateDate datetime
as
begin
	insert Booking
	(
		 UserId 
		,VehiculeId 
		,Code 
		,Status 
		,StartDate 
		,EndDate 
		,KmStart 
		,KmEnd 
		,Accompte 
		,CreateDate 
	)
	values
	(
		@UserId
	   ,@VehiculeId 
	   ,@Code 
	   ,@Status 
	   ,@StartDate 
	   ,@EndDate 
	   ,@KmStart
	   ,@KmEnd
	   ,@Accompte
	   ,@CreateDate
	)
end
go

create procedure sp_Booking_Update
	@Id int
   ,@UserId varchar(128)
   ,@VehiculeId int
   ,@Code varchar(128)
   ,@Status int
   ,@StartDate datetime
   ,@EndDate datetime
   ,@KmStart int
   ,@KmEnd int
   ,@Accompte decimal
   ,@CreateDate datetime
as
begin
	update Booking
	set  UserId = @UserId
		,VehiculeId = @VehiculeId
		,Code = @Code
		,Status = @Status
		,StartDate = @StartDate
		,EndDate = @EndDate
		,KmStart = @KmStart
		,KmEnd = @KmEnd
		,Accompte = @Accompte
		,CreateDate = @CreateDate
	where Id = @Id
end
go

create procedure sp_Booking_FindById
	@Id int
as
    select b.* 
    	  ,v.*
    	  ,m.*
    	  ,ma.*
    	  ,vc.*
    from Booking b
    inner join Vehicule v on v.Id = b.VehiculeId
    inner join Modele m on m.Id = v.ModeleId
    inner join Marque ma on ma.Id = m.MarqueId
    inner join VehiculeCategorie vc on vc.Id = m.VehiculeCategorieId
	where b.Id = @Id
go

create procedure sp_Booking_FindByCode
	@Code varchar(50)
as
	select b.* 
    	  ,v.*
    	  ,m.*
    	  ,ma.*
    	  ,vc.*
    from Booking b
    inner join Vehicule v on v.Id = b.VehiculeId
    inner join Modele m on m.Id = v.ModeleId
    inner join Marque ma on ma.Id = m.MarqueId
    inner join VehiculeCategorie vc on vc.Id = m.VehiculeCategorieId
	where b.Code = @Code
go

create procedure sp_Booking_DeleteById
	@Id int
as
	delete from Booking where Id = @Id
go

create procedure sp_Booking_DeleteByCode
	@Code varchar(50)
as
	delete from Booking where Code = @Code
go
