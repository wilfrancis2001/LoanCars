
--------------------------------------
-- 		deleting of foreign keys	--
--------------------------------------

if  exists (select * from sys.foreign_keys where object_id = OBJECT_ID(N'[dbo].[FK_AspNetUsers_UpdateUser]'))
alter table [dbo].[AspNetUsers] drop constraint [FK_AspNetUsers_UpdateUser]
go

if  exists (select * from sys.foreign_keys where object_id = OBJECT_ID(N'[dbo].[FK_AspNetUsers_CreationUser]'))
alter table [dbo].[AspNetUsers] drop constraint [FK_AspNetUsers_CreationUser]
go

if  exists (select * from sys.foreign_keys where object_id = OBJECT_ID(N'[dbo].[FK_AspNetUserClaims]'))
alter table [dbo].[AspNetUserClaims] drop constraint [FK_AspNetUserClaims]
go

if  exists (select * from sys.foreign_keys where object_id = OBJECT_ID(N'[dbo].[FK_AspNetUserLogins]'))
alter table [dbo].[AspNetUserLogins] drop constraint [FK_AspNetUserLogins]
go

if  exists (select * from sys.foreign_keys where object_id = OBJECT_ID(N'[dbo].[FK_AspNetUserRole_Role]'))
alter table [dbo].[AspNetUserRole] drop constraint [FK_AspNetUserRole_Role]
go

if  exists (select * from sys.foreign_keys where object_id = OBJECT_ID(N'[dbo].[FK_AspNetUserRole_User]'))
alter table [dbo].[AspNetUserRole] drop constraint [FK_AspNetUserRole_User]
go


--------------------------------------
-- 		deleting of tables	     	--
--------------------------------------
	 
if exists (select * from dbo.sysobjects where id = OBJECT_ID(N'[dbo].[AspNetUserClaims]'))
drop table [dbo].[AspNetUserClaims]
go

if exists (select * from dbo.sysobjects where id = OBJECT_ID(N'[dbo].[AspNetUserLogins]'))
drop table [dbo].[AspNetUserLogins]
go

if exists (select * from dbo.sysobjects where id = OBJECT_ID(N'[dbo].[AspNetUserRole]'))
drop table [dbo].[AspNetUserRole]
go

if exists (select * from dbo.sysobjects where id = OBJECT_ID(N'[dbo].[AspNetRole]'))
drop table [dbo].[AspNetRole]
go

if exists (select * from dbo.sysobjects where id = OBJECT_ID(N'[dbo].[AspNetUsers]'))
drop table [dbo].[AspNetUsers]
go 

if exists (select * from dbo.sysobjects where id = OBJECT_ID(N'[dbo].[TEST]'))
drop table [dbo].[TEST]
go

--------------------------------------
-- 		creating of tables	     	--
--------------------------------------

CREATE TABLE AspNetUsers 
(
  Id varchar(128) NOT NULL,
  Email varchar(256) NOT NULL,
  UserName varchar(256) NOT NULL,
  FirstName nvarchar(256) NULL ,
  LastName nvarchar(256) NULL,
  AccessFailedCount int NULL DEFAULT(0),
  IsActive bit NULL DEFAULT(0),
  IsDelete bit NULL DEFAULT(0),
  LockoutEnabled bit NULL DEFAULT(0),
  EmailConfirmed bit NULL DEFAULT(0),
  TwoFactorEnabled bit NULL DEFAULT(0),
  PhoneNumberConfirmed bit NULL DEFAULT(0),
  PhoneNumber varchar(256) NOT NULL,
  StreetNumber int NULL,
  StreetName varchar(256) NOT NULL,
  AdditionnalInfo varchar(256) NULL,
  BoxOffice int NOT NULL,
  Town varchar(256) NOT NULL,
  Country varchar(128) NOT NULL,
  PasswordHash varchar(256) NOT NULL,
  SecurityStamp varchar(256) NOT NULL,
  LockoutEndDateUtc datetimeoffset DEFAULT NULL,
  CreationDate datetimeoffset(2) NOT NULL DEFAULT(GETDATE()), 
  UpdateDate datetimeoffset(2) NULL, 
  CONSTRAINT PK_AspNetUsers PRIMARY KEY (Id),
  CONSTRAINT UK_AspNetUsers_Email UNIQUE(Email),
  CONSTRAINT UK_AspNetUsers_UserName UNIQUE(UserName)
);


CREATE TABLE AspNetRole 
(
  Id varchar(128) NOT NULL,
  Name varchar(256) NOT NULL,
  CONSTRAINT PK_AspNetRole PRIMARY KEY (Id),
  CONSTRAINT UK_AspNetRole_Name UNIQUE(Name)
);


CREATE TABLE AspNetUserClaims 
(
  Id int identity(1,1) NOT NULL,
  UserId varchar(128) NOT NULL,
  ClaimType varchar(256), 
  ClaimValue varchar(256),
  CONSTRAINT PK_AspNetUserClaims PRIMARY KEY (Id),
  CONSTRAINT UK_AspNetUserClaims UNIQUE (UserId,ClaimType),
  CONSTRAINT FK_AspNetUserClaims FOREIGN KEY (UserId) REFERENCES AspNetUsers(Id) ON DELETE CASCADE ON UPDATE NO ACTION
);


CREATE TABLE AspNetUserLogins 
(
  LoginProvider varchar(128) NOT NULL,
  ProviderKey varchar(128) NOT NULL,
  UserId varchar(128) NOT NULL,
  CONSTRAINT PK_AspNetUserLogins PRIMARY KEY (LoginProvider,ProviderKey,UserId),
  CONSTRAINT UK_AspNetUserLogins UNIQUE (UserId),
  CONSTRAINT FK_AspNetUserLogins FOREIGN KEY (UserId) REFERENCES AspNetUsers(Id) ON DELETE CASCADE ON UPDATE NO ACTION
);


CREATE TABLE AspNetUserRole 
(
  UserId varchar(128) NOT NULL,
  RoleId varchar(128) NOT NULL
  CONSTRAINT PK_AspNetUserRole PRIMARY KEY (UserId,RoleId),
  CONSTRAINT UK_AspNetUserRole UNIQUE(UserId),
  CONSTRAINT FK_AspNetUserRole_User FOREIGN KEY (UserId) REFERENCES AspNetUsers (Id) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT FK_AspNetUserRole_Role FOREIGN KEY (RoleId) REFERENCES AspNetRole (Id) ON DELETE CASCADE ON UPDATE NO ACTION
);


if not exists (select * from sysindexes where name='IX_AspNetUserLogins')
	CREATE UNIQUE INDEX IX_AspNetUserLogins ON AspNetUserLogins(UserId);  

if not exists (select * from sysindexes where name='IX_AspNetUserClaims')
	CREATE UNIQUE INDEX IX_AspNetUserClaims ON AspNetUserClaims(UserId); 

--if not exists (select * from sysindexes where name='IX_AspNetUserRole')
--	CREATE UNIQUE INDEX IX_AspNetUserRole ON AspNetUserRole(RoleId);  

