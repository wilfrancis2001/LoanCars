﻿using Loan.Cars.Model;
using Loan.Cars.Prerequisities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loan.Cars.Repositories
{
    public class CategorieVehiculeRepository : BaseRepository
    {
        public CategorieVehiculeRepository(System.Data.IDbConnection _con) : base(_con) { }

        public IEnumerable<CategorieVehicule> GetCategorieVehicules() 
        {
            return ExecuteSelect<CategorieVehicule>("sp_CategorieVehicule_Select", CommandType.StoredProcedure);
        }

        public CategorieVehicule GetCategorieVehiculeByCode(string code)   
        {
            return ExecuteScalar<CategorieVehicule>("sp_CategorieVehicule_FindByCode", CommandType.StoredProcedure, new { code = code });
        }

        public IEnumerable<CategorieVehicule> GetCategorieVehiculeByModelId(int id)
        {
            return ExecuteSelect<CategorieVehicule>("sp_CategorieVehicule_FindByModelId", CommandType.StoredProcedure, new { modelId = id }); 
        }

        public bool IsInCategorie(string code)
        {
            return GetCategorieVehiculeByCode(code) != null;
        }

        public void AddCategorieVehicule(CategorieVehicule cat) 
        {
            ExecuteNonQuery("sp_CategorieVehicule_Add", CommandType.StoredProcedure, new
            {
                name = cat.Name,
                code = cat.Code
            });
        }
    }
}
