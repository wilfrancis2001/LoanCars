﻿using Loan.Cars.Model;
using Loan.Cars.Prerequisities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loan.Cars.Repositories
{
    public class MarqueVehiculeRepository : BaseRepository
    {
        public MarqueVehiculeRepository(IDbConnection _con) : base(_con) { } 

        public IEnumerable<Marque> GetMarqueVehicules()
        {
            return ExecuteSelect<Marque>("sp_MarqueVehicule_Select", CommandType.StoredProcedure);
        }

        public Marque GetMarqueVehiculeByCode(string code)
        {
            return ExecuteScalar<Marque>("sp_MarqueVehicule_FindByCode", CommandType.StoredProcedure, new { code = code });
        }

        public bool IsInMarque(string code)
        {
            return GetMarqueVehiculeByCode(code) != null;
        }

        public void AddMarqueVehicule(Marque marque)
        {
            ExecuteNonQuery("sp_MarqueVehicule_Add", CommandType.StoredProcedure, new
            {
                name = marque.Name,
                code = marque.Code
            });
        }
    }
}
