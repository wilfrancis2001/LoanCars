﻿using Dapper;
using Loan.Cars.Model;
using Loan.Cars.Prerequisities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loan.Cars.Repositories
{
    public class BookingRepository : BaseRepository
    {
        public BookingRepository(System.Data.IDbConnection _con) : base(_con) { }

        public IEnumerable<Booking> GetBookings(string userId)
        {
            var result = Connection.Query<Booking,Vehicule,Modele, Marque, CategorieVehicule, Booking>("sp_Booking_Select", 
                            (booking,vehicule, model, brand, cat) =>
                            {
                                model.Marque = brand;
                                model.Categorie = cat;
                                vehicule.Modele = model;
                                booking.Vehicule = vehicule;
                                return booking;
                            }, splitOn: "Id,Id,Id,Id", commandType: CommandType.StoredProcedure, param:new { userId = userId });
            return result;
        }

        public Booking GetBookingById(int id)
        {
            var result = Connection.Query<Booking, Vehicule, Modele, Marque, CategorieVehicule, Booking>("sp_Booking_FindById",
                            (booking, vehicule, model, brand, cat) =>
                            {
                                model.Marque = brand;
                                model.Categorie = cat;
                                vehicule.Modele = model;
                                booking.Vehicule = vehicule;
                                return booking;
                            }, splitOn: "Id,Id,Id,Id", commandType: CommandType.StoredProcedure, param: new { id = id });
            return result.FirstOrDefault();
        }

        public Booking GetBookingByCode(string code) 
        {
            var result = Connection.Query<Booking, Vehicule, Modele, Marque, CategorieVehicule, Booking>("sp_Booking_FindByCode",
                            (booking, vehicule, model, brand, cat) =>
                            {
                                model.Marque = brand;
                                model.Categorie = cat;
                                vehicule.Modele = model;
                                booking.Vehicule = vehicule;
                                return booking;
                            }, splitOn: "Id,Id,Id,Id", commandType: CommandType.StoredProcedure, param: new { Code = code });
            return result.FirstOrDefault();
        }


        public void AddBooking(Booking booking)
        {
            ExecuteNonQuery("sp_Agence_Add", CommandType.StoredProcedure, new
            {
                 UserId =          booking.User
                ,VehiculeId =      booking.Vehicule
                ,Code =            booking.Code
                ,StartDate =       booking.StartDate
                ,EndDate =         booking.EndDate
                ,KmStart =         booking.KmStart
                ,KmEnd =           booking.KmEnd
                ,Accompte =        booking.Accompte
                ,Status =          booking.Status
                ,CreateDate =      booking.CreateDate
            });
        }

        public void UpdateBooking(Booking booking) 
        {
            ExecuteNonQuery("sp_Agence_Update", CommandType.StoredProcedure, new
            {
                 Id = booking.Id
                ,UserId = booking.User
                ,VehiculeId = booking.Vehicule
                ,Code = booking.Code
                ,StartDate = booking.StartDate
                ,EndDate = booking.EndDate
                ,KmStart = booking.KmStart
                ,KmEnd = booking.KmEnd
                ,Accompte = booking.Accompte
                ,Status = booking.Status
                ,UpdateDate = booking.UpdateDate 
            });
        }

        public void DeleteBooking(int id)
        {
            ExecuteNonQuery("sp_Booking_Delete", CommandType.StoredProcedure, new { Id = id }); 
        }
    }
}
