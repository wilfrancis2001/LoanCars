﻿
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Loan.Cars.Prerequisities;
using System.Data;
using Loan.Cars.Model;

namespace Loan.Cars.Repositories
{
    public class UserClaimsRepository<TUser> : BaseRepository where TUser : class, IDapperIdentity<TUser>
    {
        public UserClaimsRepository(IDbConnection _con) : base(_con) { }

        public IEnumerable<Claim> FindByUserId(string userId)
        {
            var userClaims = ExecuteSelect<UserClaim>("sp_UserClaim_FindByUserId", CommandType.StoredProcedure, new { userId = userId });
            return userClaims.Select(c => new Claim(c.ClaimType, c.ClaimValue)).ToList();
        }

        public void DeleteSingleclaim(TUser user, Claim claim)
        {
            ExecuteNonQuery("sp_UserClaim_DeleteByUserId", CommandType.StoredProcedure, new { UserId = user.Id, claimValue = claim.Value, claimType = claim.Type }); 
        }

        public void Insert(TUser user, Claim claim)
        {
            var newUserClaim = new UserClaim()
            {
                ClaimType = claim.Type,
                ClaimValue = claim.Value,
                UserId = user.Id
            };
            ExecuteNonQuery("sp_UserClaim_Add", CommandType.StoredProcedure, newUserClaim);
        }
    }
}
