﻿using Loan.Cars.Business;
using Loan.Cars.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Loan.Cars.Controllers
{
    public class VehiculeController : Controller
    {
        #region initialisation

        private VehiculeBusiness vehiculeBusiness { get; set; }
        private ModeleVehiculeBusiness modeleBusiness { get; set; }
        private MarqueVehiculeBusiness marqueBusiness { get; set; }
        private CategorieVehiculeBusiness categorieBusiness { get; set; }
        private AgenceBusiness agenceBusiness { get; set; }
        
        public VehiculeController()  
        {
            vehiculeBusiness = new VehiculeBusiness(ApplicationDbContext.Create());
            modeleBusiness = new ModeleVehiculeBusiness(ApplicationDbContext.Create());
            marqueBusiness = new MarqueVehiculeBusiness(ApplicationDbContext.Create());
            categorieBusiness = new CategorieVehiculeBusiness(ApplicationDbContext.Create());
            agenceBusiness = new AgenceBusiness(ApplicationDbContext.Create());
        }

        #endregion

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(int id)
        {
            return View();
        }

        public ActionResult Create()
        {
            var marques = marqueBusiness.GetMarqueVehicules().Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
            int marqueId = int.Parse(marques.FirstOrDefault().Value);
            var modeles = modeleBusiness.GetModeleVehiculeByMarqueId(marqueId);
            var categories = modeles.Select(x => new SelectListItem { Text = x.Categorie.Name, Value = x.Categorie.Id.ToString() });

            ViewBag.Marques = marques;
            ViewBag.Modeles = modeles.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
            ViewBag.Categories = modeles.Select(x => new SelectListItem { Text = x.Categorie.Name, Value = x.Categorie.Id.ToString() });
            ViewBag.Agences = agenceBusiness.GetAgences().Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() });

            return View();
        }

        [HttpPost]
        public ActionResult Create(VehiculeViewModel vm)
        {
            try
            {
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Edit(int id)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Edit(VehiculeViewModel vm)
        {
            try
            {
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            return RedirectToAction("Index");
            //return View();
        }

        public JsonResult GetDropDownDataModele(string id)
        {
            var modeles = modeleBusiness.GetModeleVehiculeByMarqueId(int.Parse(id));
            return Json(modeles.Select(x => new { Id = x.Id, Name = x.Name }), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDropDownDataCategorie(string id)
        { 
            var categories = categorieBusiness.GetCategorieVehiculeByModelId(int.Parse(id));
            return Json(categories.Select(x => new { Id = x.Id, Name = x.Name }), JsonRequestBehavior.AllowGet);
        }
    }
}
