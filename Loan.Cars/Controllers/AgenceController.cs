﻿using Loan.Cars.Business;
using Loan.Cars.Model;
using Loan.Cars.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Loan.Cars.Controllers
{
    public class AgenceController : BaseController
    {
        #region initialisation

        private AgenceBusiness agenceBusiness { get; set; }

        public AgenceController() 
        {
            agenceBusiness = new AgenceBusiness(ApplicationDbContext.Create());
        }

        #endregion

        public ActionResult Index()
        {
            var agences = agenceBusiness.GetAgences()
                                        .Select(x => new AgenceViewModel
                                        {
                                            Id = x.Id,
                                            Name = x.Name,
                                            StreetNumber = x.StreetNumber,
                                            StreetName = x.StreetName,
                                            AdditionnalInfo = x.AdditionAdress,
                                            BoxOffice = x.ZipCode,
                                            PhoneNumber = x.PhoneNumber,
                                            Town = x.Town,
                                            Country = x.Country
                                        });
            return View(agences);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(AgenceViewModel model)
        {
            if (ModelState.IsValid)
            {
                var agence = new Agence 
                {
                    Name = model.Name,
                    PhoneNumber = model.PhoneNumber,
                    StreetNumber = model.StreetNumber,
                    StreetName = model.StreetName,
                    ZipCode = model.BoxOffice,
                    AdditionAdress = model.AdditionnalInfo,
                    Town = model.Town,
                    Country = model.Country
                };

                try
                {
                    agenceBusiness.AddAgence(agence);
                    return RedirectToAction("Index", "Agence");
                }
                catch(Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var agence = agenceBusiness.GetAgenceById(id);
            AgenceViewModel vm = null;
            if(agence != null)
            {
                vm = new AgenceViewModel
                {
                    Id = agence.Id,
                    Name = agence.Name,
                    PhoneNumber = agence.PhoneNumber,
                    StreetNumber = agence.StreetNumber,
                    StreetName = agence.StreetName,
                    BoxOffice = agence.ZipCode,
                    AdditionnalInfo = agence.AdditionAdress,
                    Town = agence.Town,
                    Country = agence.Country
                };

                return View("Edit", vm);
            }
            return RedirectToAction("Index", "Agence");
        }

        [HttpPost]
        public ActionResult Edit(AgenceViewModel model)
        {
            if (ModelState.IsValid)
            {
                var agence = agenceBusiness.GetAgenceById(model.Id.Value); 
                agence.Name = model.Name;
                agence.StreetNumber = model.StreetNumber;
                agence.StreetName = model.StreetName;
                agence.AdditionAdress = model.AdditionnalInfo;
                agence.PhoneNumber = model.PhoneNumber;
                agence.ZipCode = model.BoxOffice;
                agence.Town = model.Town;
                agence.Country = model.Country;

                try
                {
                    agenceBusiness.UpdateAgence(agence);
                    return RedirectToAction("Index", "Agence");
                }
                catch(Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }
            return View(model);
        }

        public ActionResult Remove(int id)
        {
            try
            {
                agenceBusiness.DeleteAgences(id);
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }
            return RedirectToAction("Index", "Agence");
        }
    }
}