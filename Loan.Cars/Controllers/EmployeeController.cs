﻿using Loan.Cars.Business;
using Loan.Cars.Model.Common;
using Loan.Cars.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Loan.Cars.Controllers
{
    public class EmployeeController : BaseController
    {
        #region initialisation

        private UserBusiness userBusiness { get; set; } 
        private AgenceBusiness agenceBusiness { get; set; }

        public EmployeeController()
        {
            userBusiness = new UserBusiness(ApplicationDbContext.Create());
            agenceBusiness = new AgenceBusiness(ApplicationDbContext.Create());
        }

        #endregion

        public ActionResult Index()
        {
            var userId = User.Identity.GetUserId();
            var employees = userBusiness.GetUsers()
                                        .Where(x => x.Id != userId)
                                        .Select(x => 
                                            new EmployeeViewModel
                                            {
                                                UserId = x.Id,
                                                Email = x.Email,
                                                FirstName = x.FirstName,
                                                LastName = x.LastName,
                                                PhoneNumber = x.PhoneNumber,
                                                StreetNumber = x.StreetNumber,
                                                StreetName = x.StreetName,
                                                AdditionnalInfo = x.AdditionnalInfo,
                                                BoxOffice = x.BoxOffice,
                                                Town = x.Town,
                                                Country = x.Country
                                            })
                                        .AsEnumerable();

            return View(employees);
        }

        public ActionResult Create()
        {
            ViewBag.Agences = AgenceSelectList();
            ViewBag.Roles = RoleSelectList();
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(EmployeeViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    AgenceId = int.Parse(model.AgenceId),
                    UserName = model.Email,
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    PhoneNumber = model.PhoneNumber,
                    AccessFailedCount = 0,
                    StreetNumber = model.StreetNumber,
                    StreetName = model.StreetName,
                    BoxOffice = model.BoxOffice,
                    AdditionnalInfo = model.AdditionnalInfo,
                    Town = model.Town,
                    Country = model.Country
                };

                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await UserManager.AddToRoleAsync(user.Id, model.RoleName);
                    return RedirectToAction("Index", "Employee");
                }
                AddErrors(result);
            }
            return View(model);
        }

        public async Task<ActionResult> Remove(string id) 
        {
            var user = UserManager.FindByIdAsync(id);
            if(user != null)
                await UserManager.DeleteAsync(user.Result);

            return RedirectToAction("Index", "Employee");
        }

        public async Task<ActionResult> Edit(string id)  
        {
            var user = await UserManager.FindByIdAsync(id);
            string roleName = UserManager.GetRoles(id).FirstOrDefault();
            
            EmployeeViewModel vm = null;
            if(user != null)
            {
                vm = new EmployeeViewModel
                {
                    UserId = id,
                    AgenceId = user.AgenceId.ToString(),
                    Email = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    StreetNumber = user.StreetNumber,
                    StreetName = user.StreetName,
                    AdditionnalInfo = user.AdditionnalInfo,
                    BoxOffice = user.BoxOffice,
                    Town = user.Town,
                    Country = user.Country,
                    PhoneNumber = user.PhoneNumber,
                    RoleName = roleName,
                    Password = user.PasswordHash
                };
            }
            ViewBag.Agences = AgenceSelectList(user.AgenceId);
            ViewBag.Roles = RoleSelectList(roleName);
            return View("Edit", vm);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(EmployeeViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByIdAsync(model.UserId);
                user.UserName = model.Email;
                user.AgenceId = int.Parse(model.AgenceId);
                user.Email = model.Email;
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                user.PhoneNumber = model.PhoneNumber;
                user.AccessFailedCount = 0;
                user.StreetNumber = model.StreetNumber;
                user.StreetName = model.StreetName;
                user.BoxOffice = model.BoxOffice;
                user.AdditionnalInfo = model.AdditionnalInfo;
                user.Town = model.Town;
                user.Country = model.Country;

                var result = await UserManager.UpdateAsync(user);
                if (result.Succeeded)
                {
                    string roleName = UserManager.GetRoles(user.Id).FirstOrDefault();
                    if(roleName.ToLower() != model.RoleName.ToLower())
                    {
                        userBusiness.RemoveFromRole(model.UserId, roleName);
                        UserManager.AddToRole(model.UserId, model.RoleName);
                    }
                    return RedirectToAction("Index", "Employee");
                }
                AddErrors(result);
            }
            return View(model);
        }


        #region private methods

        private IEnumerable<SelectListItem> RoleSelectList(string selectedRole = null)
        {
            var roles = RoleManager.Roles;
            var items = new List<SelectListItem>();
            if (roles != null)
            {
                var res = roles.Where(x => x.Name.ToLower() != "basic").Select(x => new SelectListItem { Value = x.Name, Text = x.Name });
                if (!string.IsNullOrEmpty(selectedRole))
                    res.Select(x => new SelectListItem { Value = x.Value, Text = x.Text, Selected = selectedRole == x.Value });

                items.AddRange(res);
            }
            return items;
        }

        private IEnumerable<SelectListItem> AgenceSelectList(int? selectedAgenceId = null)
        {
            var items = agenceBusiness.GetAgences();

            if (selectedAgenceId.HasValue)
                return items.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name, Selected = selectedAgenceId.Value == x.Id });

            return items.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name });
        }

        #endregion
    }
}