﻿using Loan.Cars.Business;
using Loan.Cars.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Loan.Cars.Controllers
{
    public class BookingController : Controller
    {
        #region initialisation

        private BookingBusiness bookingBusiness { get; set; } 
        public BookingController()
        {
            bookingBusiness = new BookingBusiness(Models.ApplicationDbContext.Create()); 
        }

        #endregion

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(int id)
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(BookingViewModel vm)
        {
            try
            {
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Edit(int id)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Edit(BookingViewModel vm)
        {
            return RedirectToAction("Index");
            //return View();
        }

        public ActionResult Delete(int id)
        {
            return RedirectToAction("Index");
            //return View();
        }

        public ActionResult Cancel()
        {
            return RedirectToAction("Index");
            //return View();
        }
    }
}
