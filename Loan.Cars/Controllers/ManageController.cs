﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Loan.Cars.Models;

namespace Loan.Cars.Controllers
{
    [Authorize]
    public class ManageController : BaseController
    {
        public async Task<ActionResult> Index(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Votre mot de passe a été mis à jour avec succès"
                : message == ManageMessageId.UdpateAccountSucess ? "Vos informations personnelles ont été mises à jour avec succès"
                : message == ManageMessageId.Error ? "Une erreur est survenue"
                : "";

            var userInfo = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            var model = new ManageViewModel
            {
                FirstName = userInfo.FirstName,
                LastName = userInfo.LastName,
                Email = userInfo.Email,
                PhoneNumber = userInfo.PhoneNumber,
                StreetNumber = userInfo.StreetNumber,
                StreetName = userInfo.StreetName,
                AdditionnalInfo = userInfo.AdditionnalInfo,
                BoxOffice = userInfo.BoxOffice,
                Town = userInfo.Town,
                Country = userInfo.Country
            };
            return View(model);
        }

        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.Password);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                return RedirectToAction("Index", new { Message = ManageMessageId.ChangePasswordSuccess });
            }
            AddErrors(result);
            return View("Index");
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> UpdateAccount(UpdateAccountViewModel model)
        {

            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            var vm = new ManageViewModel
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                StreetNumber = user.StreetNumber,
                StreetName = user.StreetName,
                AdditionnalInfo = user.AdditionnalInfo,
                PhoneNumber = user.PhoneNumber,
                BoxOffice = user.BoxOffice,
                Town = user.Town,
                Country = user.Country,
                Email = user.Email
            };

            if (!ModelState.IsValid)
            {
                return View("Index", vm);
            }

            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.StreetNumber = model.StreetNumber;
            user.StreetName = model.StreetName;
            user.AdditionnalInfo = model.AdditionnalInfo;
            user.PhoneNumber = model.PhoneNumber;
            user.BoxOffice = model.BoxOffice;
            user.Town = model.Town;
            user.Country = model.Country;
            user.Email = model.Email;

            var result = await UserManager.UpdateAsync(user);

            if(result.Succeeded)
            {
                return RedirectToAction("Index", new { Message = ManageMessageId.UdpateAccountSucess });
            }

            AddErrors(result);
            return View("Index",vm);
        }

        #region Helpers

        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        private bool HasPhoneNumber()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PhoneNumber != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            UdpateAccountSucess,
            Error
        }

        #endregion
    }
}