﻿using Loan.Cars.Business;
using Loan.Cars.Model;
using Loan.Cars.Model.Common;
using Loan.Cars.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Web;
using System.Web.Mvc;

namespace Loan.Cars.Controllers
{
    public class BaseController : Controller
    {
        #region private properties

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;
        private ApplicationDbContext _context;

        #endregion

        public ApplicationDbContext ApplicationDbContext  
        {
            get
            {
                return _context ?? HttpContext.GetOwinContext().Get<ApplicationDbContext>(); 
            }
            protected set
            {
                _context = value;
            }
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            protected set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            protected set
            {
                _userManager = value;
            }
        }

        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            set
            {
                _roleManager = value;
            }
        }

        protected override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsAuthenticated)
            {
                if (User != null && User.Identity.IsAuthenticated)
                {
                    var id = User.Identity.GetUserId();
                    var user = UserManager.FindById(id);
                    if (user != null)
                        filterContext.HttpContext.Session[id] = $"Bonjour {user.FirstName} {user.LastName} !";
                }
            }
            base.OnAuthorization(filterContext);
        }

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            ApplicationInitializer.InitializeRoles(new RoleBusiness(ApplicationDbContext.Create()));
            ApplicationInitializer.AddDefaultAdmin(UserManager);
            ApplicationInitializer.AddCategoryVehicules(new CategorieVehiculeBusiness(ApplicationDbContext.Create()));
            ApplicationInitializer.AddMarqueVehicules(new MarqueVehiculeBusiness(ApplicationDbContext.Create()));
            ApplicationInitializer.AddModeleVehicules(
                                                        new ModeleVehiculeBusiness(ApplicationDbContext.Create()),
                                                        new MarqueVehiculeBusiness(ApplicationDbContext.Create()),
                                                        new CategorieVehiculeBusiness(ApplicationDbContext.Create())
                                                     );
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (UserManager != null)
                {
                    UserManager.Dispose();
                    UserManager = null;
                }

                if (SignInManager != null)
                {
                    SignInManager.Dispose();
                    SignInManager = null;
                }
            }

            base.Dispose(disposing);
        }


        #region Helpers

        // Used for XSRF protection when adding external logins
        protected const string XsrfKey = "XsrfId";

        protected IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        protected void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        protected ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

        //protected override void OnActionExecuted(ActionExecutedContext filterContext)
        //{
        //    var _user = UserManager.FindByIdAsync(User.Identity.GetUserId());
        //    ViewBag.FullName = _user != null && _user.Result != null ?
        //                        $"{_user.Result.FirstName} {_user.Result.LastName}" : User.Identity.Name;

        //    base.OnActionExecuted(filterContext);
        //}

        #endregion

    }

    public class ApplicationInitializer
    {
        public static void InitializeRoles(RoleBusiness business)
        {
            if (!business.IsInRole("BASIC")) business.Insert(new IdentityRole { Id = Guid.NewGuid().ToString(), Name = "BASIC" });
            if (!business.IsInRole("EMPLOYEE")) business.Insert(new IdentityRole { Id = Guid.NewGuid().ToString(), Name = "EMPLOYEE" });
            if (!business.IsInRole("ADMIN")) business.Insert(new IdentityRole { Id = Guid.NewGuid().ToString(), Name = "ADMIN" });
        }

        public static void AddDefaultAdmin(ApplicationUserManager userManager)
        {
            var user = new ApplicationUser
            {
                FirstName = "Admin",
                LastName = "Admin",
                Email = "admin@admin.com",
                UserName = "admin@admin.com",
                PhoneNumber = "0123456789",
                StreetNumber = 0,
                StreetName = "NC",
                BoxOffice = 12345,
                Town = "NC",
                Country = "NC"
            };

            var ret = userManager.CreateAsync(user, "P@ssword0");
            if (ret.Result != null && ret.Result.Succeeded)
            {
                userManager.AddToRoleAsync(user.Id, "ADMIN");
            }

        }

        public static void AddCategoryVehicules(CategorieVehiculeBusiness business)
        {
            if (!business.IsInCategorie("BERLINE")) business.AddCategorieVehicule(new CategorieVehicule { Code = "BERLINE", Name = "Berline" });
            if (!business.IsInCategorie("BREAK")) business.AddCategorieVehicule(new CategorieVehicule { Code = "BREAK", Name = "Break" });
            if (!business.IsInCategorie("UTILITAIRE")) business.AddCategorieVehicule(new CategorieVehicule { Code = "UTILITAIRE", Name = "Utilitaire" });

            /*
            if (!business.IsInCategorie("SUV")) business.AddCategorieVehicule(new CategorieVehicule { Code = "SUV", Name = "SUV" });
            if (!business.IsInCategorie("MONOSPACE")) business.AddCategorieVehicule(new CategorieVehicule { Code = "MONOSPACE", Name = "Monospace" });
            if (!business.IsInCategorie("VSPORT")) business.AddCategorieVehicule(new CategorieVehicule { Code = "VSPORT", Name = "Voiture Sport" });
            */
        }

        public static void AddMarqueVehicules(MarqueVehiculeBusiness business) 
        {
            if (!business.IsInMarque("MERCEDESBENZ" )) business.AddMarqueVehicule(new Marque { Code = "MERCEDESBENZ", Name = "MERCEDES BENZ" });
            if (!business.IsInMarque("AUDI"         )) business.AddMarqueVehicule(new Marque { Code = "AUDI", Name = "AUDI" });
            if (!business.IsInMarque("BMW"          )) business.AddMarqueVehicule(new Marque { Code = "BMW", Name = "BMW" });
            if (!business.IsInMarque("FORD"         )) business.AddMarqueVehicule(new Marque { Code = "FORD", Name = "FORD" });
            if (!business.IsInMarque("RENAULT"      )) business.AddMarqueVehicule(new Marque { Code = "RENAULT", Name = "RENAULT" });
            if (!business.IsInMarque("CITROEN"      )) business.AddMarqueVehicule(new Marque { Code = "CITROEN", Name = "CITROEN" });
            
            /*if (!business.IsInMarque("PEUGEOT"      )) business.AddMarqueVehicule(new Marque { Code = "PEUGEOT", Name = "PEUGEOT" });
            if (!business.IsInMarque("OPEL"         )) business.AddMarqueVehicule(new Marque { Code = "OPEL", Name = "OPEL" });
            if (!business.IsInMarque("TOYOTA"       )) business.AddMarqueVehicule(new Marque { Code = "TOYOTA", Name = "TOYOTA" });
            if (!business.IsInMarque("NISSAN")) business.AddMarqueVehicule(new Marque { Code = "NISSAN", Name = "NISSAN" });
            if (!business.IsInMarque("VOLVO")) business.AddMarqueVehicule(new Marque { Code = "VOLVO", Name = "VOLVO" });
            if (!business.IsInMarque("SKODA")) business.AddMarqueVehicule(new Marque { Code = "SKODA", Name = "SKODA" });
            if (!business.IsInMarque("VW")) business.AddMarqueVehicule(new Marque { Code = "VW", Name = "VolksWagen" });
            if (!business.IsInMarque("CHEVROLET")) business.AddMarqueVehicule(new Marque { Code = "CHEVROLET", Name = "CHEVROLET" });
            if (!business.IsInMarque("HYNDAI")) business.AddMarqueVehicule(new Marque { Code = "HYNDAI", Name = "HYNDAI" });
            if (!business.IsInMarque("JEEP")) business.AddMarqueVehicule(new Marque { Code = "JEEP", Name = "JEEP" });
            if (!business.IsInMarque("ISUZU")) business.AddMarqueVehicule(new Marque { Code = "ISUZU", Name = "ISUZU" });
            if (!business.IsInMarque("JAGUAR")) business.AddMarqueVehicule(new Marque { Code = "JAGUAR", Name = "JAGUAR" });
            if (!business.IsInMarque("LANDROVER")) business.AddMarqueVehicule(new Marque { Code = "LANDROVER", Name = "LANDROVER" });
            if (!business.IsInMarque("LANCIA")) business.AddMarqueVehicule(new Marque { Code = "LANCIA", Name = "LANCIA" });
            if (!business.IsInMarque("LEXUS")) business.AddMarqueVehicule(new Marque { Code = "LEXUS", Name = "LEXUS" });*/
        }

        public static void AddModeleVehicules(ModeleVehiculeBusiness mobusiness,
                                              MarqueVehiculeBusiness mabusiness, 
                                              CategorieVehiculeBusiness vcbusiness) 
        {

            var _berline = vcbusiness.GetCategorieVehiculeByCode("BERLINE");
            var _break = vcbusiness.GetCategorieVehiculeByCode("BREAK");
            var _utilitaire = vcbusiness.GetCategorieVehiculeByCode("UTILITAIRE");

            //MERCEDESBENZ
            var mercedes = mabusiness.GetMarqueVehiculeByCode("MERCEDESBENZ");
            if (mercedes != null)
            {
                if (!mobusiness.IsInModele("CLASSEC3")) mobusiness.AddModeleVehicule(new Modele { Code = "CLASSEC3", Name = "CLASSE C3", Marque = mercedes, Categorie = _berline });
                if (!mobusiness.IsInModele("CLASSEE4BREAK")) mobusiness.AddModeleVehicule(new Modele { Code = "CLASSEE4BREAK", Name = "CLASSE E4 BREAK", Marque = mercedes, Categorie = _break });
                if (!mobusiness.IsInModele("VITO")) mobusiness.AddModeleVehicule(new Modele { Code = "VITO", Name = "MERCEDES VITO", Marque = mercedes, Categorie = _utilitaire });

            }

            //AUDI
            var audi = mabusiness.GetMarqueVehiculeByCode("AUDI");
            if (audi != null)
            {
                if (!mobusiness.IsInModele("AUDIA6")) mobusiness.AddModeleVehicule(new Modele { Code = "AUDIA6", Name = "AUDI A6", Marque = audi, Categorie = _berline });
                if (!mobusiness.IsInModele("AUDIA6BREAK")) mobusiness.AddModeleVehicule(new Modele { Code = "AUDIA6BREAK", Name = "AUDI A6 BREAK", Marque = audi, Categorie = _break });
                //if (!mobusiness.IsInModele("ANTOS")) mobusiness.AddModeleVehicule(new Modele { Code = "ANTOS", Name = "ANTOS", Marque = audi, Categorie = _berline });
            }

            //BMW
            var bmw = mabusiness.GetMarqueVehiculeByCode("BMW");
            if (bmw != null)
            {
                if (!mobusiness.IsInModele("SERIEA3F30")) mobusiness.AddModeleVehicule(new Modele { Code = "SERIEA3F30", Name = "SERIE A3 F30", Marque = bmw, Categorie = _berline });
                if (!mobusiness.IsInModele("SERIE3E91TOURING")) mobusiness.AddModeleVehicule(new Modele { Code = "SERIE3E91TOURING", Name = "SERIE 3 E91 TOURING", Marque = bmw, Categorie = _break });
                //if (!mobusiness.IsInModele("CLASSEE")) mobusiness.AddModeleVehicule(new Modele { Code = "CLASSEE", Name = "CLASSE E", Marque = bmw, Categorie = _berline });
            }

            //FORD
            var ford = mabusiness.GetMarqueVehiculeByCode("FORD");
            if (ford != null)
            {
                if (!mobusiness.IsInModele("MONDEO3")) mobusiness.AddModeleVehicule(new Modele { Code = "MONDEO3", Name = "MONDEO 3", Marque = ford, Categorie = _berline });
                if (!mobusiness.IsInModele("FORDMONDEO4SW")) mobusiness.AddModeleVehicule(new Modele { Code = "FORDMONDEO4SW", Name = "FORD MONDEO 4 SW", Marque = ford, Categorie = _break });
                if (!mobusiness.IsInModele("FORDTRANSIT4")) mobusiness.AddModeleVehicule(new Modele { Code = "FORDTRANSIT4", Name = "FORD TRANSIT 4", Marque = ford, Categorie = _utilitaire });
            }

            //RENAULT
            var renault = mabusiness.GetMarqueVehiculeByCode("RENAULT");
            if (renault != null)
            {
                if (!mobusiness.IsInModele("LAGUNA3")) mobusiness.AddModeleVehicule(new Modele { Code = "LAGUNA3", Name = "LAGUNA 3", Marque = renault, Categorie = _berline });
                if (!mobusiness.IsInModele("RENAULTLAGUNA3ESTATE")) mobusiness.AddModeleVehicule(new Modele { Code = "RENAULTLAGUNA3ESTATE", Name = "RENAULT LAGUNA 3 ESTATE", Marque = renault, Categorie = _break });
                if (!mobusiness.IsInModele("MASTER2")) mobusiness.AddModeleVehicule(new Modele { Code = "MASTER2", Name = "RENAULT MASTER 2", Marque = renault, Categorie = _utilitaire });
            }

            //CITROEN
            var citroen = mabusiness.GetMarqueVehiculeByCode("CITROEN"); 
            if (citroen != null)
            {
                if (!mobusiness.IsInModele("CITROENC5")) mobusiness.AddModeleVehicule(new Modele { Code = "CITROENC5", Name = "CITROEN C5", Marque = citroen, Categorie = _berline });
                if (!mobusiness.IsInModele("CITROENC5TOURER")) mobusiness.AddModeleVehicule(new Modele { Code = "CITROENC5TOURER", Name = "CITROEN C5 TOURER", Marque = citroen, Categorie = _break });
                if (!mobusiness.IsInModele("JUMPER")) mobusiness.AddModeleVehicule(new Modele { Code = "JUMPER", Name = "CITROEN JUMPER", Marque = citroen, Categorie = _utilitaire });
            }
        }
    }
}