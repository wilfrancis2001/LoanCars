﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Loan.Cars.Startup))]
namespace Loan.Cars
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
