﻿using Loan.Cars.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Loan.Cars.Models
{
    public class VehiculeViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Catégorie")]
        public string CategorieId { get; set; } 

        [Required]
        [Display(Name = "Marque")]
        public string MarqueId { get; set; } 

        [Required]
        [Display(Name = "Modèle")]
        public string ModeleId { get; set; }

        [Required]
        [Display(Name = "Agence")]
        public string AgenceId { get; set; } 

        [Required]
        [Display(Name = "Kilometrage")]
        public int Kilometrage { get; set; }

        [Required]
        [Display(Name = "Charger une image")]
        public HttpPostedFileBase ImageFile { get; set; }

        [Required]
        [Display(Name = "Immatriculation")]
        public string Immatriculation { get; set; }

        [Display(Name = "Poids autorisé")]
        public double ChargeMax { get; set; }

        [Required]
        [Display(Name = "Date d'acquisition")]
        public DateTime DateAchat { get; set; }

        [Display(Name = "Charge utile")]
        public double ChargeUtile { get; set; }

        [Display(Name = "Climatisation")]
        public bool Climatisation { get; set; }

        [Required]
        [Display(Name = "Transmission")]
        public TransmissionTypeEnum TransmissionType { get; set; }

        [Display(Name = "Nombre de portes")]
        public byte NombrePlaces { get; set; }

        [Display(Name = "Nombre de bagages")]
        public byte NombreBagages { get; set; }
    }
}