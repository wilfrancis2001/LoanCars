﻿
using Loan.Cars.Model;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Loan.Cars.Models
{
    public class ApplicationDbContext
    {
        public static System.Data.Common.DbConnection Create()
        {
            var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            return new System.Data.SqlClient.SqlConnection(connectionString);
        }
    }
}