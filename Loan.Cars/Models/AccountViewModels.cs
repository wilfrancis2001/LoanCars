﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Loan.Cars.Models
{
    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Courrier électronique")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Mot de passe")]
        public string Password { get; set; }

        [Display(Name = "Mémoriser le mot de passe ?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel : AdresseViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Courrier électronique")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Prénom")]
        [StringLength(100, ErrorMessage = "La prénom {0} doit comporter au moins {2} caractères.", MinimumLength = 2)]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Nom")]
        [StringLength(100, ErrorMessage = "La nom {0} doit comporter au moins {2} caractères.", MinimumLength = 2)]
        public string LastName { get; set; }


        [Required]
        [StringLength(100, ErrorMessage = "La chaîne {0} doit comporter au moins {2} caractères.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Mot de passe")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmer le mot de passe ")]
        [Compare("Password", ErrorMessage = "Le mot de passe et le mot de passe de confirmation ne correspondent pas.")]
        public string ConfirmPassword { get; set; }
        
    }

    public class AdresseViewModel 
    {
        [Display(Name = "Numéro Rue")]
        [RegularExpression(@"^\d{0,5}$", ErrorMessage = "Le numéro de la rue doit être un nombre")]
        public int StreetNumber { get; set; }

        [Required]
        [Display(Name = "Nom Rue")]
        public string StreetName { get; set; }

        [Display(Name = "Complement adresse")]
        public string AdditionnalInfo { get; set; }

        [Required]
        [Display(Name = "Téléphone")]
        [RegularExpression(@"^[(]?\d{3}[)]?[-. ]?\d{3}[-. ]?\d{4}$", ErrorMessage = "Format incorrect numéro téléphone.")]
        public string PhoneNumber { get; set; }

        [Required]
        [Display(Name = "Code Postal")]
        [RegularExpression(@"^[1-9]\d{4}$", ErrorMessage = "Format incorrect code postal(Ex:12345).")]
        public int BoxOffice { get; set; }

        [Required]
        [Display(Name = "Ville")]
        public string Town { get; set; } 

        [Required]
        [Display(Name = "Pays")]
        public string Country { get; set; }
    }

}
