﻿
namespace Loan.Cars.Models
{
    public class AgenceViewModel : AdresseViewModel
    {
        public int? Id { get; set; }

        public string Name { get; set; }
    }
}