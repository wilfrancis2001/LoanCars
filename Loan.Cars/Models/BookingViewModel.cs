﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Loan.Cars.Models
{
    public class BookingViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Client")]
        public string User { get; set; }

        public VehiculeViewModel Vehicule { get; set; }

        [Required]
        [Display(Name = "N° reservation")]
        public string Code { get; set; }

        [Required]
        [Display(Name = "Début de location")]
        public DateTime StartDate { get; set; }

        [Required]
        [Display(Name = "Fin de location")]
        public DateTime EndDate { get; set; }

        [Required]
        [Display(Name = "Kilométrage au depart")]
        public int KmStart { get; set; }

        [Required]
        [Display(Name = "Kilométrage à l'arrivée")]
        public int KmEnd { get; set; }

        [Display(Name = "Accompte")]
        public double? Accompte { get; set; }

        public int Status { get; set; }

        [Required]
        [Display(Name = "Date de booking")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "Date de mise à jour")]
        public DateTime? UpdateDate { get; set; }
    }
}