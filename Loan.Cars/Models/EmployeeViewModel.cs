﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Loan.Cars.Models
{
    public class EmployeeViewModel : AdresseViewModel
    {
        public EmployeeViewModel()
        {
            UserId = Guid.NewGuid().ToString();
            Password = "P@ssword0";
        }

        [Required]
        public string UserId { get; set; }

        [Required]
        [Display(Name = "Selectionner un role")]
        public string RoleName { get; set; }

        [Required]
        [Display(Name = "Affecter une agence")]
        public string AgenceId { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Courrier électronique")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Prénom")]
        [StringLength(100, ErrorMessage = "La prénom {0} doit comporter au moins {2} caractères.", MinimumLength = 2)]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Nom")]
        [StringLength(100, ErrorMessage = "La nom {0} doit comporter au moins {2} caractères.", MinimumLength = 2)]
        public string LastName { get; set; }


        [Required]
        [StringLength(100, ErrorMessage = "La chaîne {0} doit comporter au moins {2} caractères.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Mot de passe")]
        public string Password { get; set; }
        
    }
}