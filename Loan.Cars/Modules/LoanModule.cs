﻿
using Autofac;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Compilation;

namespace Loan.Cars
{
    public class LoanModule : Module
    {
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(BuildUnityContainer);

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        }

        public static void RegisterTypes(IUnityContainer iContainer)
        {
            var allAssemblies = BuildManager
                 .GetReferencedAssemblies()
                 .Cast<System.Reflection.Assembly>();

            // Get contracts
            var contractsAssembly = allAssemblies
                .FirstOrDefault(a => a.FullName.StartsWith("Loan.Cars.Contracts"));

            var contracts = contractsAssembly
                .GetTypes()
                .Where(t => t.IsInterface)
                .Select(t => t);

            // Get types that implement contracts
            string[] assembliesToLookFor = new string[] {
                "Loan.Cars.Business",
                "Loan.Cars.Repositories",
                "Loan.Cars.Identity",
                "Loan.Cars.Infrastructure",
                "Loan.Cars.Prerequisities"
            };

            var assembiesWithImplementation = allAssemblies
               .Where(a => assembliesToLookFor.Any(aName => a.FullName.StartsWith(aName)));

            List<object> typesToRegister = null;
            foreach (var ass in assembiesWithImplementation)
            {
                var types = ass.GetExportedTypes();
                foreach (var x in types)
                {
                    foreach (var inter in contracts)
                    {
                        if (inter.IsAssignableFrom(x))
                        {
                            if (typesToRegister == null) typesToRegister = new List<object>();
                            typesToRegister.Add(new { _class = x, _interface = inter });

                            iContainer.RegisterType(inter, x);
                            break;
                        }
                    }
                }
            }
        }

        private static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }

        protected override void Load(ContainerBuilder bldr)
        {
            GetConfiguredContainer();
        }
    }
}