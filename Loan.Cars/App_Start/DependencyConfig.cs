﻿using Autofac;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Web;
using System.Web.Compilation;
using System.Web.Mvc;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;

namespace Loan.Cars
{
    [ExcludeFromCodeCoverage]
    public class DependencyConfig
    {
        #region Unity Container

        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(BuildUnityContainer);

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();
            //RegisterTypes(container);
            return container;
        }

        private static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }

        #endregion

        public static void RegisterTypes(IUnityContainer iContainer)
        {
            var allAssemblies = BuildManager
                 .GetReferencedAssemblies()
                 .Cast<System.Reflection.Assembly>();

            // Get contracts
            var contractsAssembly = allAssemblies
                .FirstOrDefault(a => a.FullName.StartsWith("Loan.Cars.Contracts"));

            var contracts = contractsAssembly
                .GetTypes()
                .Where(t => t.IsInterface)
                .Select(t => t);

            // Get types that implement contracts
            string[] assembliesToLookFor = new string[] {
                "Loan.Cars.Business",
                "Loan.Cars.Repositories",
                //"Loan.Cars.Identity",
                "Loan.Cars.Infrastructure",
                "Loan.Cars.Prerequisities"
            };

            var assembiesWithImplementation = allAssemblies
               .Where(a => assembliesToLookFor.Any(aName => a.FullName.StartsWith(aName)));

            List<object> typesToRegister = null;
            foreach (var ass in assembiesWithImplementation)
            {
                var types = ass.GetExportedTypes();
                foreach (var x in types)
                {
                    foreach (var inter in contracts)
                    {
                        if (inter.IsAssignableFrom(x))
                        {
                            if (typesToRegister == null) typesToRegister = new List<object>();
                            typesToRegister.Add(new { _class = x, _interface = inter });

                            iContainer.RegisterType(inter, x);
                            break;
                        }
                    }
                }
            }
        }

        public static void RegisterDependencies()
        {
            var container = GetConfiguredContainer();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));


            //var builder = new ContainerBuilder();
            //builder.RegisterModule(new LoanModule());
            //var container = builder.Build();

            //DynamicModuleUtility.RegisterModule(typeof(UnityPerRequestHttpModule));

        }
    }
}