﻿
//using Microsoft.Practices.Unity.Mvc;
//using System.Linq;
//using System.Web.Mvc;

//[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Loan.Cars.App_Start.UnityWebActivator), nameof(Loan.Cars.App_Start.UnityWebActivator.Start))]
//[assembly: WebActivatorEx.ApplicationShutdownMethod(typeof(Loan.Cars.App_Start.UnityWebActivator), nameof(Loan.Cars.App_Start.UnityWebActivator.Shutdown))]

//namespace Loan.Cars.App_Start
//{
//    /// <summary>Provides the bootstrapping for integrating Unity with ASP.NET MVC.</summary>
//    public static class UnityWebActivator
//    {
//        /// <summary>Integrates Unity when the application starts.</summary>
//        public static void Start()
//        {
//            var container = DependencyConfig.GetConfiguredContainer();

//            FilterProviders.Providers.Remove(FilterProviders.Providers.OfType<FilterAttributeFilterProvider>().First());
//            FilterProviders.Providers.Add(new UnityFilterAttributeFilterProvider(container));

//            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

//            // To Do : Comment if you do not want to use PerRequestLifetimeManager
//            Microsoft.Web.Infrastructure.DynamicModuleHelper.DynamicModuleUtility.RegisterModule(typeof(UnityPerRequestHttpModule));
//        }

//        /// <summary>Disposes the Unity container when the application is shut down.</summary>
//        public static void Shutdown()
//        {
//            var container = DependencyConfig.GetConfiguredContainer();
//            container.Dispose();
//        }
//    }
//}