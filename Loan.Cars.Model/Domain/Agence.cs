﻿
namespace Loan.Cars.Model
{
    public class Agence
    {
        public int? Id { get; set; }

        public string Name { get; set; }

        public int StreetNumber { get; set; }

        public string StreetName { get; set; }

        public string AdditionAdress { get; set; }

        public string PhoneNumber { get; set; }

        public int ZipCode { get; set; }

        public string Town { get; set; }

        public string Country { get; set; }
    }
}
