﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loan.Cars.Model
{
    public class Booking
    {
        public int Id { get; set; }
        public string User { get;set; }
        public Vehicule Vehicule { get; set; } 
        public string Code { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int KmStart { get;set; }
        public int KmEnd { get;set; }
        public double Accompte { get;set; } 
        public int Status { get;set; }   
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}
