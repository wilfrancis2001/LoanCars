﻿
using System;

namespace Loan.Cars.Model
{
    public enum TransmissionTypeEnum
    {
        Manuelle,
        Automatique
    }

    public class Vehicule
    {
        public int Id { get; set; }
        public Modele Modele { get; set; }
        public Agence Agence { get; set; }
        public int Kilometrage { get; set; }
        public string Immatriculation { get; set; }
        public double Capacite { get; set; }
        public double ChargeMax { get; set; }
        public DateTime DateAchat { get; set; }
        public double ChargeUtile { get; set; }
        public bool Climatisation { get; set; }
        public TransmissionTypeEnum TransmissionType { get; set; }
        public byte NombrePlaces { get; set; }
        public byte NombreBagages { get; set; }
    }
    

    public class Modele
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public Marque Marque { get; set; }
        public CategorieVehicule Categorie { get; set; }
    }

    public class Marque
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }

    public class CategorieVehicule
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
