﻿
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace Loan.Cars.Model
{
    public interface IDapperIdentity<T> : IUser where T : class, IUser<string>
    {
         int? AgenceId { get; set; }
         string Email { get; set; }
         string FirstName { get; set; }
         string LastName { get; set; }
         bool IsActive { get; set; }
         bool EmailConfirmed { get; set; }
         string PasswordHash { get; set; }
         string SecurityStamp { get; set; }

         string PhoneNumber { get; set; }
         int StreetNumber { get; set; }
         string StreetName { get; set; }
         string AdditionnalInfo { get; set; }
         int BoxOffice { get; set; }
         string Town { get; set; }
         string Country { get; set; }

         bool PhoneNumberConfirmed { get; set; }
         bool TwoFactorEnabled { get; set; }
         DateTime? LockoutEndDateUtc { get; set; }
         bool LockoutEnabled { get; set; }
         int AccessFailedCount { get; set; }
         Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<T> manager);
    }
}
