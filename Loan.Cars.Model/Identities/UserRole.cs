﻿
namespace Loan.Cars.Model
{
    //[Table("AspNetUserRole")]
    public class UserRole 
    {
        //[Key]
        public string UserId { get; set; }
        //[Key]
        public string RoleId { get; set; }

        public string RoleName { get; set; }
    }
}
