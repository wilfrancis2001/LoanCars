﻿
using System;
using Microsoft.AspNet.Identity;

namespace Loan.Cars.Model
{
    //[Table("AspNetRole")]
    public class IdentityRole : IRole
    {
        public IdentityRole()
        {
            //Id = Guid.NewGuid().ToString();
        }

        public IdentityRole(string name) : this()
        {
            Name = name;
        }

        public IdentityRole(string name, string id)
        {
            Name = name;
            Id = id;
        }
        //[Key][Required]
        public string Id { get; set; }
        
        public string Name { get; set; }
    }
}
